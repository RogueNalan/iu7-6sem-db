﻿using DocumentFilesProcessor.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;

namespace DocumentFilesProcessor.Splitters
{
    class ContentsSplitter: ISplitter
    {
        private readonly ReadOnlyCollection<ReadOnlyCollection<string>> contents;

        internal ContentsSplitter(ReadOnlyCollection<ReadOnlyCollection<string>> contents)
        {
            if (contents == null)
                throw new NullReferenceException("File contents cannot be null!");
            this.contents = contents;
        }
    
        public void SplitInto(string destDir, string filenameStart = "")
        {
            Directory.CreateDirectory(destDir);

            for (int i = 0; i < contents.Count; ++i)
            {
                using (var file = File.CreateText(Path.Combine(destDir, filenameStart + i.ToString() + ".txt")))
                    foreach (string s in contents[i])
                        file.WriteLine(s);
            }
        }
    }
}
