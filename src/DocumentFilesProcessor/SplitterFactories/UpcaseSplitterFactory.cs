﻿using DocumentFilesProcessor.Interfaces;
using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DocumentFilesProcessor.Splitters;

namespace DocumentFilesProcessor.SplitterFactories
{
    class UpcaseSplitterFactory: ISplitterFactory
    {
        public ISplitter CreateSplitter(string filename)
        {
            return new UpcaseSplitter(filename);
        }

        public ISplitter CreateSplitter(Stream stream)
        {
            return new UpcaseSplitter(stream);
        }
    }
}
