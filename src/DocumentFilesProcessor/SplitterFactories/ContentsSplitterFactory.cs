﻿using DocumentFilesProcessor.Interfaces;
using System;
using System.Configuration;
using System.IO;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Novacode;
using DocumentFilesProcessor.Splitters;

namespace DocumentFilesProcessor.SplitterFactories
{
    class ContentsSplitterFactory: ISplitterFactory
    {
        public ISplitter CreateSplitter(string filename)
        {
            using (var filestream = new FileStream(filename, FileMode.Open))
                return CreateSplitter(filestream);
        }

        public ISplitter CreateSplitter(Stream stream)
        {
            using (var doc = DocX.Load(stream))
                return new ContentsSplitter(ParseArticles(GetHeaders(doc), GetContents(doc)));
        }

        private static string[] GetContents(DocX doc)
        {
            return doc.Paragraphs.Where(par => par != null && !String.IsNullOrWhiteSpace(par.Text)).Select(p => p.Text).ToArray();
        }

        private static string[] GetHeaders(DocX doc)
        {
            var headersStart = doc.Paragraphs.FindIndex(p => p != null && p.Text != null && p.Text.Contains(ConfigReader.ContentsHeader));
            if (headersStart == -1)
                throw new ArgumentException("File doesn't contain table of contents!");

            return doc.Paragraphs.GetRange(headersStart, doc.Paragraphs.Count - headersStart)
                                 .Where(par => par != null && par.FollowingTable != null && par.FollowingTable.Rows != null)
                                 .SelectMany(par => par.FollowingTable.Rows)
                                 .Where(row => row != null && row.Paragraphs != null && row.Paragraphs[0] != null && !String.IsNullOrWhiteSpace(row.Paragraphs[0].Text))
                                 .Select(row => (row.Paragraphs[0].Text.Length < ConfigReader.MinHeaderLength) ? 
                                     row.Paragraphs[0].Text : row.Paragraphs[0].Text.Substring(0, ConfigReader.MinHeaderLength))
                                 .ToArray();
        }

        private static ReadOnlyCollection<ReadOnlyCollection<string>> ParseArticles(string[] headers, string[] contents)
        {
            if (headers.Length < 2) // Начало одной статьи и оглавление
                throw new ArgumentException("Wrong table of contents format!");

            int currIndex = 1;

            var res = new List<ReadOnlyCollection<string>>(headers.Length - 1);
            var curr = new List<string>();

            for (int i = Array.FindIndex(contents, s => s.Contains(headers[0])); i < contents.Length; ++i)
            {
                if (contents[i].Contains(headers[currIndex]))
                {
                    if (curr.Count >= ConfigReader.MinParagraphs)
                    {
                        res.Add(curr.AsReadOnly());
                        curr = new List<string>();
                    }

                    if (++currIndex >= headers.Length)
                        break;
                }
                curr.Add(contents[i]);
            }

            return res.AsReadOnly();
        }
    }
}
