﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace DocumentFilesProcessor
{
    static class ConfigReader
    {
        public static string HeaderRegex { get { return headerRegex; } }
        private static readonly string headerRegex = ConfigurationManager.AppSettings["DocumentFilesProcessor.HeaderRegex"];

        public static string ContentsHeader { get { return contentsHeader; } }
        private static readonly string contentsHeader = ConfigurationManager.AppSettings["DocumentFilesProcessor.ContentsHeader"];

        public static int MinHeaderLength { get { return minHeaderLength; } }
        private static readonly int minHeaderLength = int.Parse(ConfigurationManager.AppSettings["DocumentFilesProcessor.MinHeaderLength"]);

        public static int MinParagraphs { get { return minParagrpahs; } }
        private static readonly int minParagrpahs = int.Parse(ConfigurationManager.AppSettings["DocumentFilesProcessor.MinParagraphsInArticle"]);

        public static string[] SkipStrings { get { return skipStrings; } }
        private static readonly string[] skipStrings = ConfigurationManager.AppSettings["DocumentFilesProcessor.SkipStrings"].Split(new char[] { ',' });
    }
}
