﻿using DocumentFilesProcessor.Interfaces;
using DocumentFilesProcessor.SplitterFactories;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;
using Data.Storage;

namespace DocumentFilesProcessor
{
    public class DocumentsDirectoryProcessor
    {
        private readonly string sourceDir;

        private readonly ISplitterFactory factory = new ContentsSplitterFactory();

        private readonly Action<string, string> splitAction;

        public DocumentsDirectoryProcessor (string directoryName)
        {
            if (!Directory.Exists(directoryName))
                throw new ArgumentException("Directory not found!");

            sourceDir = directoryName;

            splitAction = (filename, destDir) => 
            {
                factory.CreateSplitter(filename).SplitInto(destDir, Path.GetFileNameWithoutExtension(filename));
            };
        }

        public void ParseDirectory()
        {
            ParseDirectory(sourceDir);
        }

        public void ParseDirectory(string destDir)
        {
            Directory.CreateDirectory(destDir);

            var files = Directory.GetFiles(sourceDir, "*.docx");

            foreach (string filename in files)
                splitAction(filename, destDir);
        }

        public void ParseDirectoryParallel()
        {
            ParseDirectoryParallel(sourceDir);
        }

        public void ParseDirectoryParallel(string destDir)
        {
            Directory.CreateDirectory(destDir);

            var files = Directory.GetFiles(sourceDir, "*.docx");

            Parallel.ForEach(files,
                filename => splitAction(filename, destDir));
        }

        public void ClearTexts()
        {
            ClearTexts(sourceDir);
        }

        public void ClearTexts(string dirName)
        {
            var filenames = Directory.GetFiles(dirName, "*.txt");

            foreach (string filename in filenames)
                ClearWrongLines(filename);
        }

        public void ClearTextsParallel()
        {
            ClearTextsParallel(sourceDir);
        }

        public void ClearTextsParallel(string dirName)
        {
            var filenames = Directory.GetFiles(dirName, "*.txt");

            Parallel.ForEach(filenames,
                filename => ClearWrongLines(filename));
        }

        public void AddToDatabase()
        {
            AddToDatabase(sourceDir);
        }

        public void AddToDatabase(string dirName)
        {
            var filenames = Directory.GetFiles(dirName, "*.txt");
            var documents = new List<Document>(filenames.Length);

            foreach (string filename in filenames)
            {
                string header;
                using (var reader = new StreamReader(filename))
                    header = reader.ReadLine();
                var contents = File.ReadAllText(filename);

                documents.Add(new Document 
                { 
                    Header = header.ToUpperInvariant(),
                    Contents = new DocContents
                    {
                        ContentsString = contents
                    }
                });
            }

            using (var context = new TextAnalyzerDb())
            {
                context.Documents.AddRange(documents);
                context.SaveChanges();
            }
        }

        private static void ClearWrongLines(string filename)
        {
            string[] contents = File.ReadAllLines(filename);

            int index = Array.FindIndex(contents, str =>
            {
                if (String.IsNullOrWhiteSpace(str))
                    return false;
                foreach (string compare in ConfigReader.SkipStrings)
                    if (str.Contains(compare))
                        return false;
                return true;
            });

            if (index > 0)
                File.WriteAllLines(filename, contents.Skip(index));
        }
    }
}
