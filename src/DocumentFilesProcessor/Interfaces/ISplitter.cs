﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DocumentFilesProcessor.Interfaces
{
    interface ISplitter
    {
        void SplitInto(string destDir, string filenameStart);
    }
}
