﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Novacode;
using DocumentFilesProcessor.Interfaces;

namespace DocumentFilesProcessor
{
    internal sealed class DocumentSplitter: ISplitter
    {
        private int filesCreated = 0;

        private readonly ReadOnlyCollection<string> documentContents;

        private readonly ReadOnlyCollection<int> separatorIndexes;

        private static readonly Regex headerRegex = new Regex
        (
            ConfigurationManager.AppSettings["DocumentFilesProcessor.HeaderRegex"],
            RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase
        );

        private static readonly int minHeaderLength = int.Parse(ConfigurationManager.AppSettings["DocumentFilesProcessor.MinHeaderLength"]);

        private static readonly int minParagraphs = int.Parse(ConfigurationManager.AppSettings["DocumentFilesProcessor.MinParagraphsInArticle"]);

        internal DocumentSplitter(string filename)
            : this (new FileStream(filename, FileMode.Open), true)
        {
        }

        internal DocumentSplitter(Stream filestream)
            : this (filestream, false)
        {
        }

        public void SplitInto(string directoryName, string filenameStart = "")
        {
            if ((separatorIndexes == null) || (separatorIndexes.Count < 2)) // 0 - empty document, 1 - no headers found
                return;

            Directory.CreateDirectory(directoryName);

            for (int i = 1; i < separatorIndexes.Count; ++i)
                CreateFile
                (
                    Path.Combine(directoryName, filenameStart + filesCreated.ToString() + ".txt"),
                    separatorIndexes[i - 1], separatorIndexes[i]
                );
        }

        private DocumentSplitter(Stream source, bool sourceDispose)
        {
            using (var document = DocX.Load(source))
            {
                documentContents = document.Paragraphs.Where(p => !String.IsNullOrWhiteSpace(p.Text))
                                                      .Select(p => p.Text)
                                                      .ToList()
                                                      .AsReadOnly();
            }

            if (sourceDispose)
                source.Dispose();

            var temp = new List<int>();
            for (int i = 0; i < documentContents.Count; ++i)
                if (CheckHeader(documentContents[i]))
                    temp.Add(i);
            temp.Add(documentContents.Count - 1);
            separatorIndexes = temp.AsReadOnly();
        }

        private static bool CheckHeader(string str)
        {
            var parts = headerRegex.Matches(str);
            if (parts.Count == 0)
                return false;

            var sb = new StringBuilder();
            foreach (var part in parts)
                sb.Append(part);
            var res = sb.ToString();

            return (!String.IsNullOrWhiteSpace(res)) && (res.Length > minHeaderLength) && (res.ToUpperInvariant() == res);
        }

        private void CreateFile(string filename, int start, int end)
        {
            if (end - start < minParagraphs)
                return;

            using (var file = File.CreateText(filename))
            {
                for (int i = start; i < end; ++i)
                    file.WriteLine(documentContents[i]);

                ++filesCreated;
            }
        }
    }
}
