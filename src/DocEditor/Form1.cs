﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Data.Storage;
using Data.Models;

namespace DocEditor
{
	public partial class Form1 : Form
	{
		TextAnalyzerDb context = new TextAnalyzerDb();
		public Form1()
		{
			InitializeComponent();

			foreach ( Document d in context.Documents.Where( doc => doc.Header.Contains("\n")).ToArray() )
			{
				listBox1.Items.Add( d.Id );
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			if ( listBox1.SelectedItem == null )
			{
				MessageBox.Show( "Выбери док, да?)" );
				return;
			}

			context.Documents.Find( (int)listBox1.SelectedItem ).Header = textBox1.Text;
			context.SaveChanges();
		}

		private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			Document d = context.Documents.FirstOrDefault( doc => doc.Id == (int)listBox1.SelectedItem );
			textBox1.Text = d.Header;
			textBox2.Text = d.Contents.ContentsString;
		}

		private void textBox1_TextChanged(object sender, EventArgs e)
		{
			textBox1.Text = textBox1.Text.ToUpper();
		}
	}
}
