﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Data.Models;
using Data.Storage;
using TermsProvider.Interfaces;

namespace TermsProvider
{
    public class MainTermsProvider : IDisposable, ITermsProvider
    {
        private readonly ITermsParser parser;

        private readonly ITermFactor factorStrategy;

        private readonly TextAnalyzerDb context;
       
        private readonly IQueryable<Document> documents;

        private readonly int documentsCount;

        private static readonly Regex termEmptyTester = new Regex(@"[\s\d]+", RegexOptions.Compiled);

        private const double minFactor = 1e-7;

        private const int splitLength = 2048;

        public MainTermsProvider(ITermsParser parser, ITermFactor factorStrategy)
        {
            if ((parser == null) || (factorStrategy == null))
                throw new ArgumentNullException("Parser or factory strategy cannot be null!");

            this.parser = parser;
            this.factorStrategy = factorStrategy;

            context = new TextAnalyzerDb();
            context.Configuration.AutoDetectChangesEnabled = false; // оповещение об изменении данных
                                                                    // отключение увеличивает производительность на два порядка
            documents = context.Documents.AsNoTracking().OrderBy(doc => doc.Id);
            documents.Load();
            documentsCount = documents.Count();
        }

        public void FillTerms()
        {
            var tokens = ParseDocuments();
            var terms = GetTerms(tokens);
            foreach (var term in terms)
                term.Set = parser.CreatedSet;

            context.Terms.AddRange(terms);
            context.SaveChanges();

            var factors = GetFactors(tokens, terms);
            context.TermFactors.AddLongRange(factors.Where(factor => Math.Abs(factor.TermFactor) >= minFactor));
            context.SaveChanges();
        }

        public virtual void Dispose()
        {
            parser.Dispose();
            context.Dispose();
        }

        /// <summary>
        /// Перекодирует данные из кодировки документа в стандартную для системы и распарсивает, сохраняя порядок
        /// </summary>
        /// <returns>
        /// Коллекция найденных терминов для каждого документа в том же порядке
        /// </returns>
        private IEnumerable<IDictionary<string, int>> ParseDocuments()
        {
            var res = new List<IDictionary<string, int>>(documentsCount);
            foreach (var doc in documents)
                res.Add(parser.ParseText(doc.Contents.ContentsString));
            return res;
        }

        /// <summary>
        /// Извлекает набор терминов из токенов.
        /// Работа основывается на предположении, что все строки имеют длину, меньше или равную максимально разрешённой
        /// </summary>
        private IEnumerable<Term> GetTerms(IEnumerable<IDictionary<string, int>> tokens)
        {
            int minDocs = (int) Math.Ceiling(ConfigReader.MinPercentOfDocuments * documentsCount);
            int maxDocs = (int) Math.Ceiling(ConfigReader.MaxPercentOfDocuments * documentsCount);

            Func<string, bool> checker = term => (term.Length >= ConfigReader.MinTermLength) && (!termEmptyTester.IsMatch(term));

            // Выбираем все термы, удовлетворяющие условиям
            var res = tokens.AsParallel()
                            .SelectMany(dict => dict.Keys)
                            .Where(checker)
                            .Distinct()
                            .Select(term => new Term
                            {
                                Value = term,
                                DocumentFactor = tokens.Count(dict => dict.ContainsKey(term))
                            })
                            .Where(source => (source.DocumentFactor >= minDocs) && (source.DocumentFactor <= maxDocs))
                            .ToList();

            // Добавляем самый часто упоминаемый терм из документа, если он не содержит ни одного
            foreach (var dict in tokens)
                if ((dict.Count != 0) && (!res.Any(term => dict.ContainsKey(term.Value))))
                {
                    var toAdd = dict.Aggregate((max, curr) => ((curr.Value > max.Value) && checker(curr.Key)) ? max : curr).Key; // reduce для поиска
                    if (checker(toAdd))
                        res.Add(new Term
                        {
                            Value = toAdd,
                            DocumentFactor = tokens.Count(docDict => docDict.ContainsKey(toAdd))
                        });
                }

            return res;
        }

        private IEnumerable<DocumentTermFactor> GetFactors(IEnumerable<IDictionary<string, int>> documentTokens, IEnumerable<Term> terms)
        {
            if (documentTokens.Count() != documentsCount)
                throw new ArgumentException("Wrong number of DocumentTokens!");

            var documentFactors = new List<IEnumerable<DocumentTermFactor>>(documentsCount);
            using (var docEnum = documents.GetEnumerator())
            using (var tokEnum = documentTokens.GetEnumerator())
            {
                while (docEnum.MoveNext() && tokEnum.MoveNext())
                    documentFactors.Add(factorStrategy.GetFactors(terms, tokEnum.Current, docEnum.Current.Id, documentsCount));
            }

            return documentFactors.SelectMany(factors => factors);
        }
    }
}
