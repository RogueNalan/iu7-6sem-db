﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace TermsProvider
{
    static class ConfigReader
    {
        public static char[] SentenceSeparators { get { return sentenceSeparators; } }
        private static readonly char[] sentenceSeparators = ConfigurationManager.AppSettings["TermsProvider.SentenceSeparators"].ToCharArray();

        public static double MinPercentOfDocuments { get { return minPercentOfDocuments; } }
        private static readonly double minPercentOfDocuments = double.Parse(ConfigurationManager.AppSettings["TermsProvider.MinPercentOfDocuments"]) / 100;

        public static double MaxPercentOfDocuments { get { return maxPercentOfDocuments; } }
        private static readonly double maxPercentOfDocuments = double.Parse(ConfigurationManager.AppSettings["TermsProvider.MaxPercentOfDocuments"]) / 100;

        public static int MinTermLength { get { return minTermLength; } }
        private static readonly int minTermLength = int.Parse(ConfigurationManager.AppSettings["TermsProvider.MinTermLength"]);
    }
}
