﻿using System;
using System.Collections.Generic;
using System.Linq;
using Data.Models;
using TermsProvider.Interfaces;

namespace TermsProvider
{
    public class FrequencyFactorStrategy : ITermFactor
    {
        public double GetFactor(int termFactor, int documentFactor, int documentCount)
        {
            return termFactor * Math.Log10((double) documentCount / documentFactor);
        }


        public IEnumerable<DocumentTermFactor> GetFactors(IEnumerable<Term> terms, IDictionary<string, int> termFactors, int documentId, int documentsCount)
        {
            var res = terms.Select(term =>
                           {
                               int temp;
                               int termFactor = (termFactors.TryGetValue(term.Value, out temp)) ? temp : 0;

                               return new DocumentTermFactor
                               {
                                   DocumentId = documentId,
                                   TermId = term.Id,
                                   TermFactor = GetFactor(termFactor, term.DocumentFactor, documentsCount)
                               };
                           })
                           .ToList();

            Normalize(res);
            return res;
        }

        private void Normalize(IList<DocumentTermFactor> factors)
        {
            double sum = Math.Sqrt(factors.Sum(factor => factor.TermFactor * factor.TermFactor));

            for (int i = 0; i < factors.Count; ++i)
                factors[i].TermFactor /= sum;
        }
    }
}
