﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Data.Models;

namespace TermsProvider.Interfaces
{
    public interface ITermFactor
    {
        double GetFactor(int termFactor, int documentFactor, int documentsCount);
        IEnumerable<DocumentTermFactor> GetFactors(IEnumerable<Term> terms, IDictionary<string, int> termFactors, int documentId, int documentsCount);
    }
}
