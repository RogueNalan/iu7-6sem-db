﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TermsProvider.Interfaces
{
    public interface ITermsProvider
    {
        void FillTerms();
    }
}
