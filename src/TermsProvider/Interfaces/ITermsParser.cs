﻿using Data.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace TermsProvider.Interfaces
{
    public interface ITermsParser : IDisposable
    {
        TermSet CreatedSet { get; }

        string ParseWord(string word);
        IEnumerable<string> ParseSentence(string sentence);
        IDictionary<string, int> ParseText(string text);
        IDictionary<string, int> ParseText(Stream text);
    }
}
