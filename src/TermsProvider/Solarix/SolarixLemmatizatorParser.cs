﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using Data.Models;
using SolarixLemmatizatorEngineNET;
using TermsProvider.Interfaces;

namespace TermsProvider.Solarix
{
    public sealed class SolarixLemmatizatorParser : ITermsParser
    {
        private readonly IntPtr lemmatizator = LemmatizatorEngine.sol_LoadLemmatizatorW(lemmatizatorDbName, LemmatizatorEngine.LEME_FASTEST);

        private const char defaultSeparator = ' ';

        private static readonly string lemmatizatorDbName = "lemmatizer.db";

        private static readonly Encoding lemmatizatorEncoding = Encoding.GetEncoding("windows-1251");

        private static readonly long lemmatizatorDbSize = new FileInfo(lemmatizatorDbName).Length;

        public TermSet CreatedSet { get { return termSet; } }
        private readonly TermSet termSet = new TermSet() { TermExtractionAlgorithmId = ExtractionAlgorithmType.SolarixLemmatization };

        public SolarixLemmatizatorParser()
        {
            GC.AddMemoryPressure(lemmatizatorDbSize);
        }

        public string ParseWord(string word)
        {
            return ParseWord(word, Encoding.Default);
        }

        public string ParseWord(string word, Encoding sourceEncoding)
        {
            var lemmaSource = Convert(sourceEncoding, lemmatizatorEncoding, word);
            var sb = new StringBuilder(Term.MaxLength);
            LemmatizatorEngine.sol_GetLemmaW(lemmatizator, lemmaSource, sb, Term.MaxLength);
            return Convert(lemmatizatorEncoding, sourceEncoding, sb.ToString());
        }

        public IEnumerable<string> ParseSentence(string sentence)
        {
            return ParseSentence(sentence, defaultSeparator);
        }

        public IEnumerable<string> ParseSentence(string sentence, char wordSeparator = defaultSeparator)
        {
            return ParseSentence(sentence, Encoding.Default, wordSeparator);
        }

        public IEnumerable<string> ParseSentence(string sentence, Encoding sourceEncoding, char wordSeparator = defaultSeparator)
        {
            IntPtr lemmas = IntPtr.Zero;
            try
            {
                var lemmaSource = Convert(sourceEncoding, lemmatizatorEncoding, sentence);
                lemmas = LemmatizatorEngine.sol_LemmatizePhraseW(lemmatizator, lemmaSource, 0, wordSeparator);

                var sb = new StringBuilder(Term.MaxLength);
                int count = LemmatizatorEngine.sol_CountLemmas(lemmas);
                for (int i = 0; i < count; ++i)
                {
                    LemmatizatorEngine.sol_GetLemmaStringW(lemmas, i, sb, Term.MaxLength);
                    yield return Convert(lemmatizatorEncoding, sourceEncoding, sb.ToString());
                    sb.Clear();
                }
            }
            finally
            {
                if (lemmas != IntPtr.Zero)
                    LemmatizatorEngine.sol_DeleteLemmas(lemmas);
            }
        }

        public IDictionary<string, int> ParseText(string text)
        {
            var sentences = text.Split(ConfigReader.SentenceSeparators);

            var res = new Dictionary<string, int>();
            foreach (string sentence in sentences)
            {
                var lemmas = ParseSentence(new string(sentence.Select(c => (Char.IsPunctuation(c) || (c == '\r') || (c == '\n')) ? defaultSeparator : c).ToArray()));
                foreach (string lemma in lemmas)
                    if (res.ContainsKey(lemma))
                        ++res[lemma];
                    else
                        res.Add(lemma, 1);
            }
            return res;
        }

        public IDictionary<string, int> ParseText(Stream text)
        {
            using (var sr = new StreamReader(text))
                return ParseText(sr.ReadToEnd());
        }

        public void Dispose()
        {
            LemmatizatorEngine.sol_DeleteLemmatizator(lemmatizator);
            GC.RemoveMemoryPressure(lemmatizatorDbSize);
            GC.SuppressFinalize(this);
        }

        ~SolarixLemmatizatorParser()
        {
            LemmatizatorEngine.sol_DeleteLemmatizator(lemmatizator);
            GC.RemoveMemoryPressure(lemmatizatorDbSize);
        }

        private static string Convert(Encoding from, Encoding to, string src)
        {
            if ((from == null) || (to == null))
                throw new ArgumentNullException("Encodings cannot be null!");

            var srcBytes = from.GetBytes(src);
            var dstBytes = Encoding.Convert(from, to, srcBytes);

            return to.GetString(dstBytes);
        }
    }
}
