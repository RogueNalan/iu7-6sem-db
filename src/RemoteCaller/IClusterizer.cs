﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;

namespace RemoteCaller
{
    [ServiceContract]
    public interface IClusterizer
    {
        [OperationContract]
        void KMeans(int maxClusters, int maxIterations);

        [OperationContract]
        void Hierarchic(int minClusters);
    }
}
