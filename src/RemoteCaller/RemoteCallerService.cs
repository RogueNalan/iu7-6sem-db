﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ClustersProvider;
using ClustersProvider.ClusterizationAlgorithms;
using ClustersProvider.Comparators;
using TermsProvider;
using TermsProvider.Solarix;

namespace RemoteCaller
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class RemoteCallerService : ITermsExtractor, IClusterizer
    {
        public void ExtractTerms()
        {
            using (var prov = new MainTermsProvider(new SolarixLemmatizatorParser(), new FrequencyFactorStrategy()))
                prov.FillTerms();
        }

        public void KMeans(int maxClusters, int maxIterations)
        {
            using (var prov = new ClusterProvider())
                prov.Clusterize(new KMeansAlgorithm(new SquareEuclidianCompare(), new RandomSeeder(), maxClusters, maxIterations));
        }

        public void Hierarchic(int minClusters)
        {
            using (var prov = new ClusterProvider())
            {
                var comparator = new SquareEuclidianCompare();
                prov.Clusterize(new AgglomerativeHierarchicAlgorithm(comparator, new FarthestClusterCompare(comparator), minClusters));
            }
        }
    }
}
