﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;

namespace Graphite
{
	public class Node
	{
		public Vector2 pos;
		public float size = 10;
		public List<Node> links = new List<Node>();
		public object data = null;
		public bool isPhantom = false;

		public Node(float x, float y)
		{
			pos = new Vector2( x, y );
		}
	}
	public class Graph
	{
		public List<Node> nodes = new List<Node>();

		public Node FindAt(Vector2 pos)
		{
			return nodes.Find( n =>
				( (n.pos - pos).Length <= n.size )
			);
		}
		public Node FindAt(float x, float y)
		{
			return FindAt( new Vector2( x, y ) );
		}
		
		public static
		Graph Test()
		{
			List<Node> nodes = new List<Node>(){
				new Node( 0, 0 ),
				new Node( 50, 0 ),
				new Node( 10, 70 ),
				new Node( -120, 30 ),
				new Node( 50, -150 ),

				new Node( 20, 100 ),
				new Node( 120, 10 ),
				new Node( 100, 50 ),
				new Node( 150, 150 ),
			};
			nodes[0].links = new List<Node>()
			{
				nodes[1], nodes[2], nodes[3], nodes[4]
			};
			nodes[5].links = new List<Node>()
			{
				nodes[0], nodes[2], nodes[6], nodes[7]
			};
			nodes[6].links = new List<Node>()
			{
				nodes[5], nodes[1]
			};
			Graph g = new Graph();
			g.nodes = nodes;
			return g;
		}
	}
}
