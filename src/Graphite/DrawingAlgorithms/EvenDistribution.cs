﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;

namespace Graphite.DrawingAlgorithms.ForceDriven
{
	public class EvenDistribution : AbstractAlgorithm
	{
		float optimalDistance, c = 0.5f;
		float repelForce = 0.1f;
		float attractForce = 0.1f;
		float temp, tempDec;
		float w,h;

		Dictionary<Node, Force> forces;
			
		public EvenDistribution(Graph g, float width = 1000.0f, float height = 1000.0f)
			: base(g)
		{
			optimalDistance = c * (float)Math.Sqrt( width*height / g.nodes.Count );
			forces = new Dictionary<Node, Force>( g.nodes.Count );
				
			temp = width / 10f;
			tempDec = temp / 25f;
			w = width;
			h = height;
		}

		void AttractConnected(Vector2 A, Vector2 B, out Force fA, out Force fB)
		{
			Vector2 AB = B - A, BA = A - B;
			float distance = AB.Length;
			float scalar = -attractForce * distance*distance / optimalDistance; //притяжение
			fA = new Force( BA, scalar );
			fB = new Force( AB, scalar );
		}

		void RepelAll(Vector2 A, Vector2 B, out Force fA, out Force fB)
		{
			Vector2 AB = B - A, BA = A - B;
			float distance = AB.Length;
			float scalar = repelForce * optimalDistance*optimalDistance/distance;
			fA = new Force( BA, scalar );
			fB = new Force( AB, scalar );
		}

		protected override void Iterate()
		{
			foreach ( Node n in g.nodes )
				forces[n] = new Force();

			//repel all nodes from each other
			foreach ( Node A in g.nodes )
			{
				foreach ( Node B in g.nodes.Where( n => n != A ) )
				{
					Force fA, fB;
					RepelAll( A.pos, B.pos, out fA, out fB );
					forces[A] += fA;
					forces[B] += fB;
				}
			}

			//attract connected nodes
			foreach ( Node A in g.nodes )
			{
				foreach ( Node B in A.links )
				{
					Force fA, fB;
					AttractConnected( A.pos, B.pos, out fA, out fB );
					forces[A] += fA;
					forces[B] += fB;
				}
			}

			foreach ( var pair in forces )
			{
				//limit max displacement to temperature t and prevent from displacement outside frame
				Force f_or = pair.Value;

				Vector2 fd = f_or.Direction;
				float fs = f_or.Scalar, t = temp, m = Math.Min( fs, t );
				Force f = new Force( fd, m );

				f.Apply( ref pair.Key.pos );
				

				/*pair.Key.pos.X = Math.Min( w/2f, Math.Max( -w/2f, p.X ) );
				pair.Key.pos.Y = Math.Min( h/2f, Math.Max( -h/2f, p.Y ) );*/

			}

			temp -= tempDec;
		}

		protected override bool ShouldStop()
		{
			return temp <= 0;
		}
	}
}
