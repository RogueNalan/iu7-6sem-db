﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;


namespace Graphite.DrawingAlgorithms
{
	namespace ForceDriven
	{
		//сила применяемая к точке
		class Force
		{
			const float forceOffset = 1f; //дистанция на которую смещается точка под влиянием силы скаляром 1

			public Vector2 Direction { get; private set; }
			public float Scalar { get; private set; }

			public Force()
			{
				Direction = new Vector2();
				Scalar = 0;
			}
			public Force(Vector2 direction, float scalar)
			{
				Direction = direction.Normalized();
				float a = Direction.Length;
				Scalar = scalar;
			}
			public Force(Vector2 forceVector)
			{
				Scalar = forceVector.Length;
				Direction = forceVector.Normalized();
			}
			public void Apply(ref Vector2 point)
			{
				point += Direction * Scalar * forceOffset;
			}

			// TODO: чото както хтоничненько, ктулху одобряе, нужно подумать над чемто получше
			public static
			Force operator +(Force left, Force right)
			{
				Vector2 v = left.Direction*left.Scalar + right.Direction*right.Scalar;
				return new Force( v );
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	public abstract class AbstractAlgorithm
	{
		protected readonly Graph g;
		private bool centered = false;

		protected AbstractAlgorithm(Graph g)
		{
			this.g = g;
		}

		void Center()
		{
			if ( centered )
				return;

			Vector2 center = new Vector2();
			foreach ( Node n in g.nodes )
			{
				center += n.pos;
			}
			center /= g.nodes.Count;
			foreach ( Node n in g.nodes )
			{
				n.pos -= center;
			}
			centered = true;
		}

		/// Действия по изменению координат вершин графа
		protected abstract void Iterate();

		/// Условие, при достижении которого прекращаются итерации
		protected abstract bool ShouldStop();

		/// Основная функция с которой взаимодействует внешний мир; возвращает false когда граф достигает "равновесия"
		public bool Apply()
		{
			if ( !ShouldStop() )
			{
				Iterate();
				return true;
			}
			Center();
			return false;
		}
	}
}
