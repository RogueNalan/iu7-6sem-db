﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using OpenTK;


namespace Graphite.DrawingAlgorithms.ForceDriven
{
	public class SpringElectrical : AbstractAlgorithm
	{
		int repeats = 100;
		int repeated = 0;

		float edgeRigidForce = -10f, //минус - потому что при расстоянии больше оптимального, вершины должны сближаться
			edgeOptimalLength = 100f,
			verticesRepelForce = 50f;


		public SpringElectrical(Graph g)
			: base(g)
		{}

		void SpringEdge(Vector2 A, Vector2 B, out Force fA, out Force fB)
		{
			Vector2 AB = B - A, BA = A - B;
			float distance = AB.Length;
			float scalar = edgeRigidForce * (float)Math.Log( distance / edgeOptimalLength );
			fA = new Force( BA, scalar );
			fB = new Force( AB, scalar );
		}

		void RepelNodes(Vector2 A, Vector2 B, out Force fA, out Force fB)
		{
			Vector2 AB = B - A, BA = A - B;
			float distance = AB.Length;
			float scalar = verticesRepelForce / (distance*distance);
			fA = new Force( BA, scalar );
			fB = new Force( AB, scalar );
		}

		protected override void Iterate()
		{
			Dictionary<Node, Force> forces = new Dictionary<Node, Force>( g.nodes.Count );
			foreach ( Node n in g.nodes )
				forces[n] = new Force();


			//replace each edge with a spring to form a mechanical system
			foreach ( Node A in g.nodes )
			{
				foreach ( Node B in A.links )
				{
					Force fA, fB;
					SpringEdge( A.pos, B.pos, out fA, out fB );
					forces[A] += fA;
					forces[B] += fB;
				}
			}

			//secondly, we make nonadjacent vertices repel each other
			foreach ( Node A in g.nodes )
			{
				foreach ( Node B in g.nodes.Where(
					n =>
						n != A &&
						!A.links.Contains( n ) )
				)
				{
					Force fA, fB;
					RepelNodes( A.pos, B.pos, out fA, out fB );
					forces[A] += fA;
					forces[B] += fB;
				}
			}

			//calculate the force on each vertex; move the vertex
			foreach ( var p in forces )
			{
				p.Value.Apply( ref p.Key.pos );
			}
				
			++repeated;
		}

		protected override bool ShouldStop()
		{
			return repeated >= repeats;
		}
	}
}
