﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Drawing;

using OpenTK;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;

namespace Graphite
{
	//камера и всё что с ней связано, включая возможность переводить координаты из экранных в мировые
	//////////////////////////////////////////////////////////////////////////
	//ну подставил, получил стандартный VAшный сепаратор, иы?)
	class Camera
	{
		int x = 0, y = 0;
		
		float mouseSens = 1.0f;

		const float minScale = 0.1f, maxScale = 2.0f;
		float scaleFactor = 1.0f;

		//приватные переменные оставлены, automatic properties don't allow you to set an initial value
		public int X { get { return x; } }
		public int Y { get { return y; } }
		public float ScaleFactor { get { return scaleFactor; } }

		public void Move(int _dx, int _dy)
		{
			float dx = _dx * mouseSens / scaleFactor;
			float dy = _dy * mouseSens / scaleFactor;

			x += (int)dx;
			y += (int)dy;
		}
		public void SetPos(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
		public void ScaleInc(float scaleFactorInc)
		{
			float s = scaleFactor + scaleFactorInc;
			SetScale( s );
		}
		public void SetScale(float s)
		{
			if ( s < minScale )
				s = minScale;
			else if ( s > maxScale )
				s = maxScale;

			scaleFactor = s;
		}

		public Vector2 TranslateCoord(int mouseX, int mouseY)
		{
			Vector2 p = new Vector2( (mouseX/scaleFactor + x), (mouseY/scaleFactor + y) );
			return p;
		}

		public override string ToString()
		{
			return String.Format( "Cam:  {0} | {1}; Z={2}", x, y, scaleFactor );
		}
	}

	//отрисовка всего что нужно
	public class Graphone
	{
		static Graphone singleton = null;

		static readonly Color
			NODE_FILL = Color.Green,
			PHANTOM_FILL = Color.Goldenrod,
			NODE_LINE = Color.SpringGreen,
			PHANTOM_LINE = Color.Yellow,
			SELECT_LINE = Color.Red,
			TEXT_COLOR = Color.Ivory,
			EDGE_COLOR = Color.Aquamarine;


		OpenTK.GLControl control = null;
		int w,h;

		public Graph g = Graph.Test();

		Vector2 lastClick = new Vector2();
		Node selectedNode = null;

		Camera cam = new Camera();

		bool _ready = false;
		public bool Ready { get { return _ready; } }

		//инстанцирование синглтона
		public static Graphone I
		{
			get
			{
				if ( singleton == null )
					singleton = new Graphone();
				return singleton;
			}
		}

		//инициализирование графона ГЛконтролом на формочке, в котором всё и будет отображаться
		public void Init(OpenTK.GLControl _control)
		{
			control = _control;
			_ready = true;
			Resize();

			GL.ClearColor( Color.MidnightBlue );
			GL.LineWidth( 2.0f );
			GL.PointSize( 5.0f );
		}
		
		//изменение разрешающей способности графона в зависимости от растяжения ГЛконтрола
		public void Resize()
		{
			//а то повадились тут, бросать евент ресайза до евента лоада
			if ( !Ready )
				return;

			w = control.Width;
			h = control.Height;

			GL.MatrixMode( MatrixMode.Projection );
			GL.LoadIdentity();
			GL.Ortho( -w/2, w/2, -h/2, h/2, 0.0, 4.0 );
			GL.Viewport( 0, 0, w, h );
		}

		public void MoveCam(int dx, int dy)
		{
			cam.Move( dx, dy );
		}
		public void ZoomCam(int factorInc)
		{
			cam.ScaleInc( factorInc > 0 ? 0.1f : -0.1f );
		}

		//переотрисовка всего что нужно когда нужно
		public void ReDraw()
		{
			GL.Clear( ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit );

			// TODO: переехать с камеры на проекционную матрицу?
			GL.MatrixMode( MatrixMode.Modelview );
			GL.LoadIdentity();
			GL.Scale( cam.ScaleFactor, cam.ScaleFactor, cam.ScaleFactor );
			GL.Translate( -cam.X, -cam.Y, 0 );
			
			DrawGraph();

			if ( selectedNode != null )
			{
				if ( selectedNode.data is Data.Models.Document )
				{
					var d = (Data.Models.Document)selectedNode.data;
					Font font = new Font( FontFamily.GenericSansSerif, 14.0f );
					var printer = new OpenTK.Graphics.TextPrinter();
					printer.Begin();
					printer.Print( String.Format( "Document: {0}", d.Id ), font, TEXT_COLOR );
					font = new Font( FontFamily.GenericSansSerif, 12.0f );
					printer.Print( String.Format( "\n{0}", d.Header ), font, TEXT_COLOR);
					printer.End();
				}
			}

			control.SwapBuffers();
		}

		public void MouseClick(int x, int y, MouseButtons button)
		{
			if ( button == MouseButtons.Left )
			{
				x -= w/2;
				y = -(y - h/2);
				lastClick = cam.TranslateCoord( x, y );
				selectedNode = g.FindAt( lastClick );
			}
		}


#region Drawing	=	=	=	=	=	=	=	=	=	=	=	=	=	=	=

		//отрисовка круга вокруг точки, быстрый алгоритм с http://slabode.exofire.net/circle_draw.shtml
		static int PredictCirclePointsNum(double r)
		{
			return (int)Math.Round( 4.0 * Math.Sqrt( r ) );
		}
		static void DrawCircle(double cx, double cy, double r)
		{
			int num_segments = PredictCirclePointsNum( r );
			double theta = 2.0 * Math.PI / (double)num_segments;
			double c = Math.Cos( theta );//precalculate the sine and cosine
			double s = Math.Sin( theta );
			double t;

			double x = r;//we start at angle = 0
			double y = 0;

			for ( int ii = 0; ii < num_segments; ++ii )
			{
				GL.Vertex2( x + cx, y + cy );//output vertex

				//apply the rotation matrix
				t = x;
				x = c * x - s * y;
				y = s * t + c * y;
			}
		}

		//отрисовка грани между двумя вершинами
		static Quaternion qrot1 = Quaternion.FromMatrix( Matrix3.CreateRotationZ( 0.3f ) );
		static Quaternion qrot2 = Quaternion.FromMatrix( Matrix3.CreateRotationZ( -0.3f ) );
		static void DrawEdge(Node a, Node b)
		{
			Vector2 start = a.pos, end = new Vector2( b.pos );
			Vector2 arrow = (start-end).Normalized();
			end += arrow * b.size; //конечной точкой будет не центр узла, а его край

			//делаем > стрелочки
			arrow = Vector2.Multiply( arrow, 15f );
			Vector2 part1 = Vector2.Transform( arrow, qrot1 );
			Vector2 part2 = Vector2.Transform( arrow, qrot2 );

			GL.Vertex2( start );
			GL.Vertex2( end );

			GL.Vertex2( end );
			GL.Vertex2( end+part1 );
			GL.Vertex2( end );
			GL.Vertex2( end+part2 );
		}

		//отрисовка вершины
		static void DrawNode(Node n)
		{
			Graphone.DrawCircle( n.pos.X, n.pos.Y, n.size );
		}

		//отрисовка графа при наличии оного
		void DrawGraph()
		{
			if ( g == null )
				return;

			GL.LineWidth( 1.0f );
			GL.Color3( EDGE_COLOR );
			GL.Begin( PrimitiveType.Lines );
			foreach ( Node n in g.nodes.Where( a => a.links.Count > 0 ) )
			{
				foreach ( Node adj in n.links )
				{
					DrawEdge( n, adj );
				}
			}
			GL.End();

			GL.LineWidth( 2.0f );
			foreach ( Node n in g.nodes )
			{
				//заливка
				GL.Color3( n.isPhantom ? PHANTOM_FILL : NODE_FILL );
				GL.Begin( PrimitiveType.Polygon );
				DrawNode( n );
				GL.End();

				//обводка
				if ( n == selectedNode )
					GL.Color3( SELECT_LINE );
				else
					GL.Color3( n.isPhantom ? PHANTOM_LINE : NODE_LINE );
				GL.Begin( PrimitiveType.LineLoop );
				DrawNode( n );
				GL.End();
			}
		}
#endregion
	}
}
