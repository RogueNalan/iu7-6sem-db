﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


public class Stringworks
{
	//триммит строку, убирает все табуляции, множественные пробелы заменяет на единичные, различные вариации символов приводит к одной
	public static
	string NormalizeString(string str)
	{
		string res = str.Trim();
		res = res.Replace( '\t', ' ' );
		res = res.Replace( ' ', ' ' ); //это не пробел на пробел!! это char(A0) на char(20) !! какой-то вордовский символ, маскирующийся под пробел
		res = res.Replace( 'ё', 'е' ); //....это просто вот фейспалм в три руки
		res = res.Replace( 'Ё', 'Е' ); //..и это тоже
		res = Regex.Replace( res, @"^[\d \.\[\]]*", "" );
		res = Regex.Replace( res, "[–—]", "-" );
		res = Regex.Replace( res, "[“”«»]", "\"" );
		res = Regex.Replace( res, "  +", " " );
		return res;
	}

	static readonly Regex
	authors = new Regex(
		@"(^(([а-яА-Я\-]+ [А-Я]\.( ?[А-Я]\.)?,? )|([А-Я]\.( ?[А-Я]\.)? ?[а-яА-Я\-]+[,\.]? ))+)|(Под ред(\.|акцией).*$)" //в случае подредакции, авторы в начале не указываются
		, RegexOptions.Compiled ),
	publishers = new Regex(
		@"(([\.,] [\-:""]+)|(/+)).*"
		, RegexOptions.Compiled ),
	publishers2 = new Regex(
		@"(( Пер\.)|( Перевод)|( М\.)|( Москва)|( СПб\.)|( Сб(\.|орник))).*"
		, RegexOptions.Compiled ),
	/*publishers3 = new Regex(
		@"([а-я]+). [a-zA-Z]+.*"
		, RegexOptions.Compiled ),*/
	remains = new Regex(
		@"(. [0-9][0-9][0-9][0-9] ?.?\.?$)|([""])|( *\-$)|((http://)|(www\.)).*"
		, RegexOptions.Compiled );

	public static string[] ExtractReferences(IEnumerable<string> referencesTextArray )
	{
		var l = new List<string>();

		foreach(string str in referencesTextArray )
		{
			string refstr = NormalizeString( str );

			if ( string.IsNullOrWhiteSpace( refstr ) )
				continue;

			if ( String.IsNullOrWhiteSpace( authors.Match( refstr ).Value ) )
				continue;

			refstr = authors.Replace( refstr, "" );

			refstr = publishers.Replace( refstr, "" );
			refstr = publishers2.Replace( refstr, "" );
			//refstr = publishers3.Replace( refstr, "$1" );

			refstr = remains.Replace( refstr, "" );

			//ради "1. Купер А. // Психбольница в руках пациентов." в 85м
			if ( !String.IsNullOrWhiteSpace(refstr) )
				l.Add(refstr.ToUpperInvariant());
		}

		return l.ToArray();
	}

	//////////////////////////////////////////////////////////////////////////
		
	static int levenshteinDistance(string src, string dst)
	{
		// between same strings == 0
		if ( src == dst )
			return 0;

		int m = src.Length;
		int n = dst.Length;

		// between string S and '' == len(S)
		if ( m == 0 )
			return n;
		if ( n == 0 )
			return m;

		// matrix[i,j] for all i,j will contain Levenshtein distance between
		// first I characters of src and first J characters of dst
		// note tha matrix size is m+1 x n+1
		++m; ++n;
		int[,] matrix = new int[m, n];
		for ( int i = 0; i < m; ++i )
			matrix[i, 0] = i;
		for ( int i = 0; i < n; ++i )
			matrix[0, i] = i;

		for ( int i = 1; i < m; ++i )
			for ( int j = 1; j < n; ++j )
			{
				int cost;
				if ( src[i-1] == dst[j-1] )
					cost = 0;
				else
					cost = 1;
				int above = matrix[i-1, j];
				int left = matrix[i, j-1];
				int diag = matrix[i-1, j-1];
				matrix[i, j] = Math.Min(
					Math.Min( above+1, left+1 ),
					diag+cost );
			}
		return matrix[m-1, n-1];
	}
		
	public static double AdiffersFromB(string a, string b)
	{
		a = a.ToUpperInvariant().Trim();
		b = b.ToUpperInvariant().Trim();
		double r = (double)levenshteinDistance( a, b ) / (double)b.Length;
		return r;
	}

	//////////////////////////////////////////////////////////////////////////

	static bool StringContainsRefereneceList(string str)
	{
		if ( String.IsNullOrWhiteSpace(str) )
			return false;

		//нас не интересуют строки на 1000 символов где посреди текста может случайно
		//встретиться слово "список" или "источник"
		str = str.Substring( 0, Math.Min(50,str.Length) );

		Regex regex = new Regex(
			//в одном из доков ещё встречается как "Библиография", но при этом в другом доке "Библиография. ..." это название источника, лол
			@"^[\s\t]*(Литература)|(Список.*литературы)|(Источник)|(Список.*источников)"
			,RegexOptions.IgnoreCase | RegexOptions.Compiled);
		if ( regex.IsMatch( str ) )
			return true;
        return false;
	}

	public static int? GetReferenceListStart(string[] text)
	{
		for ( int i = text.Length-1; i > 0; --i )
			if ( StringContainsRefereneceList( text[i] ) )
				return i;
		return null;
	}
}
