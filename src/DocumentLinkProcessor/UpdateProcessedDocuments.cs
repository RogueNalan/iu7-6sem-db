﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace DocumentLinkProcessor
{
	public class UpdateProcessedDocs : IUpdate
	{
		const string c_ProgressText = "Обработано документов:";
		static int total = 0;
		static int processed = 0;
		static string text = "";

		public UpdateProcessedDocs()
		{
			System.Threading.Interlocked.Increment( ref processed );
		}
		public void Update(Form form)
		{
			ProgressBar pb = (ProgressBar)(form.Controls.Find( "progressBar", true )[0]);
			pb.Value = Convert.ToInt32( (double)processed / total * 100.0 );

			Label lbl = (Label)(form.Controls.Find( "lblProgressText", true )[0]);
			lbl.Text = String.Format( "{0} {1}/{2}", text, processed, total );
			lbl.Invalidate();
		}

		public static void ResetCount(IProgress<IUpdate> progress, int docsCount, string progressText = c_ProgressText)
		{
			total = docsCount;
			processed = -1;
			text = progressText;

			progress.Report( new UpdateProcessedDocs() );
		}
	}
}
