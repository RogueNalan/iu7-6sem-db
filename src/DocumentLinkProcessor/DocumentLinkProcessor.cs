﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Data.Models;
using Data.Storage;

using System.Windows.Forms;

using Graphite;

namespace DocumentLinkProcessor
{
    public class DocumentLinkProcessor : IDisposable
    {
		IProgress<IUpdate> progress;
		Form appForm;
		private readonly TextAnalyzerDb context = new TextAnalyzerDb();

		public DocumentLinkProcessor(Form form)
		{
			this.progress = new Progress<IUpdate>( UpdateStatus );
			this.appForm = form;
			UpdateLog.ResetLogTimer();
		}

		public void Dispose()
		{
			context.Dispose();
		}

		void UpdateStatus(IUpdate u)
		{
			u.Update( appForm );
		}

		void SaveChanges()
		{
			progress.Report( new UpdateLog( "Сохраняем изменения..." ) );
			context.SaveChanges();
			progress.Report( new UpdateLog( "Готово" ) );
		}

		//////////////////////////////////////////////////////////////////////////

		Document FindDocByHeader(string header)
		{
			object mutex = new object();
			Document result = null;
			Parallel.ForEach( context.Documents.Local, ((doc, state) =>
			{
				double lenRatio = Math.Abs( (double)header.Length / doc.Header.Length - 1.0 );

				//нет смысла сравнивать строки, различающиеся по длине на >20%
				if ( lenRatio < 0.2 && Stringworks.AdiffersFromB( header, doc.Header ) < 0.1 )
				{
					lock ( mutex )
					{
						result = doc;
						state.Stop();
					}
				}
			}
			) );
			return result;
		}

		//////////////////////////////////////////////////////////////////////////

		void doMagic_thread()
		{
			progress.Report( new UpdateLog( "Очищаем списки ссылок... " ) );
			/* Артем Столяренко:
			только тут и только сейчас*/
			context.Database.ExecuteSqlCommand( "TRUNCATE TABLE docs.DocumentReferences" );
			//да, это ДЕЙСТВИТЕЛЬНО быстрее чем foreach (var doc in docs) { doc.references.clear(); doc.citations.clear(); }
			progress.Report( new UpdateLog( "Обновляем документы... " ) );
			(context as System.Data.Entity.Infrastructure.IObjectContextAdapter)
				.ObjectContext.Refresh( System.Data.Entity.Core.Objects.RefreshMode.StoreWins,
					context.Documents.Local ); //чтобы в ужезагруженных документах не осталось висячих ссылок
			SaveChanges();

			progress.Report( new UpdateLog( "Уничтожаем существующие фантомы... " ) );
			/*context.Documents.RemoveRange( context.Documents.Where( d => d.Contents.Contents == null ) );
			SaveChanges();
			не знаю что ефу тут не нравится и знать не хочу*/
			context.Database.ExecuteSqlCommand( "DELETE FROM docs.Documents WHERE Contents IS null" );

			progress.Report( new UpdateLog( "Получаем корпус документов... " ) );
			List<Document> docs = context.Documents.Where( d => d.ReferencesText != null ).ToList();
			UpdateProcessedDocs.ResetCount( progress, docs.Count() );


			progress.Report( new UpdateLog( "Начинаем обработку документов" ) );

			foreach ( Document doc in docs )
			{
				progress.Report( new UpdateLog( String.Format( "Обрабатываем документ №{0}", doc.Id ) ) );
				
				foreach ( string refHeader in doc.ReferencesTextStringArray )
				{
					Document refDoc = FindDocByHeader( refHeader );
					string logString;
					if ( refDoc == null )
					{
						refDoc = new Document();
						refDoc.Header = refHeader;
						refDoc.Contents = new DocContents();
						refDoc.Citations = new List<Document>();
						refDoc.References = new List<Document>();
						refDoc.IsPhantom = true;
						context.Documents.Add( refDoc );
						logString = String.Format( "\tСоздан фантом <{0}>", refHeader );
					}
					else
						logString = String.Format( "\tНайдена ссылка на документ №{0} <{1}>{2}", refDoc.Id, refHeader, refDoc.IsPhantom ? ", фантом" : "" );
					
					refDoc.Citations.Add( doc );
					doc.References.Add( refDoc );
					progress.Report( new UpdateLog( logString ) );
				}
				progress.Report( new UpdateProcessedDocs() );
				context.SaveChanges(); //после каждого документа, чтобы ждать по чуть-чуть, вместо того чтобы провисать после обработки всего при запихивании воооттакенного пакета в базу
			}
			SaveChanges();
		}

		public Task doMagic()
		{
			return Task.Factory.StartNew( () => doMagic_thread(),
										TaskCreationOptions.LongRunning );
		}
		//////////////////////////////////////////////////////////////////////////

		void findRefpos_thread()
		{
			progress.Report( new UpdateLog( "Получаем корпус документов... " ) );
			List<Document> docs = context.Documents.Where( c => c.Contents.Contents != null ).ToList();
			progress.Report( new UpdateLog( "Готово" ) );
			UpdateProcessedDocs.ResetCount( progress, docs.Count );

			progress.Report( new UpdateLog( "Начинаем обработку документов" ) );

			string docIdFormat = "D"+((int)Math.Log10(docs.Count)+1);
			//foreach ( Document doc in docs )
			object mutex = new object();
			docs.AsParallel().ForAll( doc =>
			{
				string[] docText;
				lock(mutex) //чтобы ЕФ не вешался от четырёх потоков, разом пытающихся лениво выгрузить контент
				{
					docText = doc.Contents.ContentsStringArray;
				}
				int? pos = Stringworks.GetReferenceListStart( docText );
				string logString = String.Format( "[{0}]: ", doc.Id.ToString( docIdFormat ) );
				if ( pos.HasValue )
				{
					logString += String.Format( "список начинается с {0} строки (<{1}>)", pos.Value, docText[pos.Value] );
					string[] refs = Stringworks.ExtractReferences( docText.Skip( pos.Value ) );
					if ( refs.Length == 0 )
					{
						logString += Environment.NewLine + "> ВНИМАНИЕ! < Список был найден, но не содержит ни одного элемента! Замена на null";
						refs = null;
					}
					doc.ReferencesTextStringArray = refs;
				}
				else
					logString += "список не найден";
				progress.Report( new UpdateLog( logString ) );
				progress.Report( new UpdateProcessedDocs() );
			}
			);
			SaveChanges();
		}

		public Task findRefpos()
		{
			return Task.Factory.StartNew( () => findRefpos_thread(),
										TaskCreationOptions.LongRunning );
		}

		//////////////////////////////////////////////////////////////////////////

		Graph CreateReferenceGraph_thread()
		{
			progress.Report( new UpdateLog( "Получаем корпус документов... " ) );
			var docs = context.Documents.Where( d => d.References.Count > 0 || d.Citations.Count > 0 );
			progress.Report( new UpdateLog( "Готово" ) );
			UpdateProcessedDocs.ResetCount( progress, docs.Count() );

			Graph g = new Graph();
			Random rand = new Random();
			Dictionary<int, Node> nodeDict = new Dictionary<int, Node>(); //для быстрого доступа к вершине по номеру её документа

			progress.Report( new UpdateLog( "Создаём вершины графа..." ) );
			foreach ( Document doc in docs )
			{
				Node n = new Node( (float)rand.NextDouble()*200, (float)rand.NextDouble()*200 );
				n.isPhantom = doc.IsPhantom;
				n.data = doc;
				g.nodes.Add( n );
				nodeDict[doc.Id] = n;
				progress.Report( new UpdateProcessedDocs() );
			}
			progress.Report( new UpdateLog( "Готово" ) );

			progress.Report( new UpdateLog( "Определяем связи для вершин... " ) );
			var edges = docs.Where( d => d.References.Count > 0 );
			UpdateProcessedDocs.ResetCount( progress, edges.Count() );
			//docs/**/.AsParallel().ForAll( doc => // штрааааф нельзя в мультитреде в .References лениво лазить, вашужмать
			foreach ( Document doc in edges )
			{
				foreach ( Document r in doc.References )
					nodeDict[doc.Id].links.Add( nodeDict[r.Id] );
				progress.Report( new UpdateProcessedDocs() );
			}
			//);
			progress.Report( new UpdateLog( "Готово" ) );

			return g;
		}

		public Task<Graph> CreateReferenceGraph()
		{
			return Task.Factory.StartNew( () => CreateReferenceGraph_thread(),
										TaskCreationOptions.LongRunning );
		}
    }
}
