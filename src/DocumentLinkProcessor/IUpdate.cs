﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace DocumentLinkProcessor
{
	public interface IUpdate
	{
		void Update(Form form);
	}
}
