﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Forms;

namespace DocumentLinkProcessor
{
	public class UpdateLog : IUpdate
	{
		static System.Diagnostics.Stopwatch timer = new System.Diagnostics.Stopwatch();

		string str;

		public UpdateLog(string logString)
		{
			str = logString;
		}

		public static void ResetLogTimer()
		{
			timer.Restart();
		}

		public void Update(Form form)
		{
			TextBox log = (TextBox)(form.Controls.Find( "txtLogger", true )[0]);
			log.AppendText( String.Format( "{0}  | {1}{2}", timer.Elapsed, str, Environment.NewLine ) );
			//log.Invalidate();
		}
	}
}
