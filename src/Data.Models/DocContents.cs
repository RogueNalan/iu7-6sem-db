﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models
{
    [Table("Documents", Schema = "docs")]
    public class DocContents
    {
        [Key]
        public int Id { get; set; }

        public byte[] Contents
        {
            get
            {
                return contents;
            }
            set
            {
                contents = value;
                contentsString = null;
                contentsStringArray = null;
            }
        }
        private byte[] contents;

        public Document Document { get; set; }

        [NotMapped]
        public string ContentsString
        {
            get
            {
                if (Contents == null)
                    return null;
                if (contentsString == null)
                    contentsString = Document.DecodeToString(Contents);
                return contentsString;
            }
            set
            {
                Contents = Document.Encode(value);
            }
        }
        private string contentsString = null;

        [NotMapped]
        public string[] ContentsStringArray
        {
            get
            {
                if (Contents == null)
                    return null;
                if (contentsStringArray == null)
                    contentsStringArray = Document.DecodeToStringArray(Contents);
                return contentsStringArray;
            }
            set
            {
                Contents = Document.Encode(value);
            }
        }
        private string[] contentsStringArray = null;
    }
}
