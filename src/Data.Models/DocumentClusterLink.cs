﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Data.Models.Clusters;

namespace Data.Models
{
    [Table("DocumentClusterLinks")]
    public class DocumentClusterLink
    {
        [Key]
        [Column(Order=1)]
        public int DocumentId { get; set; }

        [Key]
        [Column(Order=2)]
        public int ClusterId { get; set; }

        public double? GradeOfMembership { get; set; }

        [ForeignKey("DocumentId")]
        public virtual Document Document { get; set; }

        [ForeignKey("ClusterId")]
        public virtual Cluster Cluster { get; set; }
    }
}
