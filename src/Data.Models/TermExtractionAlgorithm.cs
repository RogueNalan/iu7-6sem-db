﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models
{
    [Table("Algorithms", Schema="terms")]
    public class TermExtractionAlgorithm
    {
        [Key]
        public ExtractionAlgorithmType Id { get; set; }

        [Required]
        [StringLength(200)]
        [Index(IsUnique=true)]
        public string Description { get; set; }
    }
}
