﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models.Clusters
{
    [Table("Sets", Schema="clusterization")]
    public class ClusterSet
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Column("AlgorithmId")]
        public ClusterizationAlgorithmType ClusterizationAlgorithmId { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [ForeignKey("ClusterizationAlgorithmId")]
        public virtual ClusterizationAlgorithm Algorithm { get; set; }

        public virtual ICollection<Cluster> Clusters { get; set; }
    }
}
