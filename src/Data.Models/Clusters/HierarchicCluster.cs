﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models.Clusters
{
    public class HierarchicCluster : Cluster
    {
        public int? ReferenceId { get; set; }

        [ForeignKey("ReferenceId")]
        public virtual HierarchicCluster Reference { get; set; }
    }
}
