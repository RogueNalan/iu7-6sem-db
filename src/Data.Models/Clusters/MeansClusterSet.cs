﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Data.Models.Clusters
{
    public class MeansClusterSet : ClusterSet
    {
        [Required]
        public int ClustersNumber { get; set; }
    }
}
