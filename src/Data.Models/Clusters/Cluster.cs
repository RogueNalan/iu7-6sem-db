﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Models.Clusters
{
    [Table("Clusters", Schema="clusterization")]
    public class Cluster
    {
        [Key]
        public int Id { get; set; }
        
        [Required]
        public int ClusterSetId { get; set; }
        
        [StringLength(200)]
        public string Description { get; set; }

        [ForeignKey("ClusterSetId")]
        public virtual ClusterSet ClusterSet { get; set; }

        public virtual ICollection<DocumentClusterLink> DocumentLinks { get; set; }
    }
}
