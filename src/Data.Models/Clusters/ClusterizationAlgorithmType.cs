﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data.Models.Clusters
{
    public enum ClusterizationAlgorithmType
    {
        Hierarchic = 0,
        Kmeans,
        Cmeans
    }
}
