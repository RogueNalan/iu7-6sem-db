﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models.Clusters
{
    [Table("Algorithms", Schema="clusterization")]
    public class ClusterizationAlgorithm
    {
        [Key]
        public ClusterizationAlgorithmType Id { get; set; }

        [StringLength(200)]
        [Required]
        [Index(IsUnique=true)]
        public string Description { get; set; }

        [Required]
        public bool IsHierarchic { get; set; }

        [Required]
        public bool IsFuzzy { get; set; }

        public virtual ICollection<ClusterSet> Sets { get; set; }
    }
}
