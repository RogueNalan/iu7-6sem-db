﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models
{
    [Table("DocumentTermFactors")]
    public class DocumentTermFactor
    {
        [Key]
        [Column(Order=1)]
        public int DocumentId { get; set; }

        [Key]
        [Column(Order=2)]
        public int TermId { get; set; }

        [Required]
        public double TermFactor { get; set; }

        [ForeignKey("DocumentId")]
        public virtual Document Document { get; set; }

        [ForeignKey("TermId")]
        public virtual Term Term { get; set; }
    }
}
