﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models
{
    [Table("Terms", Schema="terms")]
    public class Term
    {
        [Key]
        public int Id { get; set; }

        [Required]
        public int TermSetId { get; set; }

        [StringLength(maxLength)]
        [Required]
        public string Value { get; set; }

        public virtual ICollection<DocumentTermFactor> Factors { get; set; }

        [ForeignKey("TermSetId")]
        public virtual TermSet Set { get; set; }

        [NotMapped]
        public int DocumentFactor { get; set; }

        [NotMapped]
        public static int MaxLength { get { return maxLength; } }
        private const int maxLength = 50;
    }
}
