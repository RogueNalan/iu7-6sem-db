﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models
{
    [Table("Documents", Schema="docs")]
    public class Document
    {
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Заголовок документа. Длина автоматически обрезается до максимально разрешённой с удалением всех лишних пробелов.
        /// </summary>
        [Required]
        [StringLength(maxHeaderLength)]
        [Index(IsUnique=true)]
        public string Header
        {
            get
            {
                return header;
            }
            set
            {
                if (!String.IsNullOrWhiteSpace(value))
                {
                    string trimmedValue = value.Trim();
                    if (trimmedValue.Length < MaxHeaderLength)
                        header = trimmedValue;
                    else
                        header = trimmedValue.Substring(0, MaxHeaderLength - 1);
                }
            }
        }
        private string header = null;

        [Required]
        public virtual DocContents Contents { get; set; }

        public byte[] ReferencesText
        {
            get
            {
                return referencesText;
            }
            set
            {
                referencesText = value;
                referencesTextString = null;
                referencesTextStringArray = null;
            }
        }
        private byte[] referencesText;

        [Required]
        public bool IsPhantom { get; set; }

        public virtual ICollection<DocumentClusterLink> ClusterLinks { get; set; }

        public virtual ICollection<DocumentTermFactor> Factors { get; set; }
        
        /// <summary>
        /// Документы, на которые ссылается этот документ
        /// </summary>
        public virtual ICollection<Document> References { get; set; }

        /// <summary>
        /// Документы, которые ссылаются на этот документ
        /// </summary>
        public virtual ICollection<Document> Citations { get; set; }

        [NotMapped]
        public string ReferencesTextString
        {
            get
            {
                if (ReferencesText == null)
                    return null;
                if (referencesTextString == null)
                    referencesTextString = DecodeToString(ReferencesText);
                return referencesTextString;
            }
            set
            {
                ReferencesText = Encode(value);
            }
        }
        private string referencesTextString = null;

        [NotMapped]
        public string[] ReferencesTextStringArray
        {
            get
            {
                if (ReferencesText == null)
                    return null;
                if (referencesTextStringArray == null)
                    referencesTextStringArray = DecodeToStringArray(ReferencesText);
                return referencesTextStringArray;
            }
            set
            {
                ReferencesText = Encode(value);
            }
        }
        private string[] referencesTextStringArray = null;

        [NotMapped]
        public static int MaxHeaderLength { get { return maxHeaderLength; } }
        private const int maxHeaderLength = 200;
        
        [NotMapped]
        public static Encoding DocumentEncoding { get { return documentEncoding; } }
        private static readonly Encoding documentEncoding = Encoding.UTF8;

        internal static string DecodeToString(byte[] bytes)
        {
            return (bytes == null) ? null : DocumentEncoding.GetString(bytes);
        }

        internal static string[] DecodeToStringArray(byte[] bytes)
        {
            return (bytes == null) ? null : DecodeToString(bytes)
                .Split(Environment.NewLine.ToCharArray())
                .Where(s => !String.IsNullOrWhiteSpace(s))
                .ToArray();
        }

        internal static byte[] Encode(string s)
        {
            return (s == null) ? null : DocumentEncoding.GetBytes(s);
        }

        internal static byte[] Encode(IEnumerable<string> sList)
        {
            return (sList == null) ? null : Encode(String.Join(Environment.NewLine, sList));
        }

    }
}
