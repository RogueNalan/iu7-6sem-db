﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;

namespace Data.Models
{
    [Table("Sets", Schema="terms")]
    public class TermSet
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [Column("AlgorithmId")]
        public ExtractionAlgorithmType TermExtractionAlgorithmId { get; set; }

        [StringLength(200)]
        public string Description { get; set; }

        [ForeignKey("TermExtractionAlgorithmId")]
        public virtual TermExtractionAlgorithm Algorithm { get; set; }

        public virtual ICollection<Term> Terms { get; set; }
    }
}
