﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Data.Storage;
using ClustersProvider.ClusterizationAlgorithms;
using Data.Models;
using Data.Models.Clusters;

namespace ClustersProvider
{
    public class ClusterProvider : IDisposable
    {
        private readonly TextAnalyzerDb context;

        public ClusterProvider()
        {
            context = new TextAnalyzerDb();
            context.Configuration.AutoDetectChangesEnabled = false; // performance

            context.Documents.Load();
            foreach (var doc in context.Documents)
                doc.Factors = new Collection<DocumentTermFactor>(doc.Factors.OrderBy(fac => fac.TermId).ToList());
        }

        public void Dispose()
        {
            context.Dispose();
        }

        public void Clusterize(AbstractClusterizationAlgorithm algorithm)
        {
            var clusters = algorithm.Clusterize(context.Documents);
            foreach (var cluster in clusters)
                cluster.ClusterSet = algorithm.CreatedSet;
            context.Clusters.AddRange(clusters);
            context.SaveChanges();
        }
    }
}
