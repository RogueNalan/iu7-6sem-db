﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ClustersProvider.Interfaces;
using Data.Models;
using Data.Models.Clusters;

namespace ClustersProvider.ClusterizationAlgorithms
{
    public sealed class AgglomerativeHierarchicAlgorithm : AbstractClusterizationAlgorithm
    {
        private readonly IClusterCompare clusterComparator;

        private readonly int minClusters;

        public AgglomerativeHierarchicAlgorithm(IDocumentCompare comparator, IClusterCompare clusterComparator, int minClusters)
            : base(comparator)
        {
            if (clusterComparator == null)
                throw new ArgumentNullException("ClusterComparator cannot be null!");

            if (minClusters < 1)
                throw new ArithmeticException("MinClusters must be at least 1!");

            this.clusterComparator = clusterComparator;
            this.minClusters = minClusters;
        }

        public override ClusterSet CreatedSet { get { return createdSet; } }
        private readonly ClusterSet createdSet = new ClusterSet { ClusterizationAlgorithmId = ClusterizationAlgorithmType.Hierarchic };

        public override IEnumerable<Cluster> Clusterize(IEnumerable<Document> documents)
        {
            var clusters = new List<HierarchicCluster>(documents.Count());
            foreach (var doc in documents)
            {
                var cluster = new HierarchicCluster();
                var link = new DocumentClusterLink
                {
                    Document = doc,
                    Cluster = cluster
                };
                var temp = new List<DocumentClusterLink>(1);
                temp.Add(link);
                cluster.DocumentLinks = new Collection<DocumentClusterLink>(temp);
                clusters.Add(cluster);
            }
            return Clusterize(clusters);
        }

        public IEnumerable<Cluster> Clusterize(IEnumerable<HierarchicCluster> sources)
        {
            var clustersList = sources.ToList();
            var currClusters = clustersList.ToList();

            while (currClusters.Count > minClusters)
            {
                var nearest = FindNearest(currClusters);
                var newCluster = UniteClusters(currClusters[nearest.xInd], currClusters[nearest.yInd]);

                if (nearest.xInd < nearest.yInd)
                    --nearest.yInd;
                currClusters.RemoveAt(nearest.xInd);
                currClusters.RemoveAt(nearest.yInd);
                currClusters.Add(newCluster);
                clustersList.Add(newCluster);
            }
            clustersList.Reverse();
            return clustersList;
        }

        private NearestIndexes FindNearest(IList<HierarchicCluster> clusters)
        {
            if (clusters.Count < 2)
                throw new ArithmeticException("Number of clusters to choose cannot be less than 2!");

            int xIndex = 0, yIndex = 1;
            double minDist = clusterComparator.Distance(clusters[xIndex], clusters[yIndex]);
            for (int i = 0; i < clusters.Count; ++i)
                for (int j = i + 1; j < clusters.Count; ++j)
                {
                    double currDist = clusterComparator.Distance(clusters[i], clusters[j]);
                    if (currDist < minDist)
                    {
                        minDist = currDist;
                        xIndex = i;
                        yIndex = j;
                    }
                }
            return new NearestIndexes
            {
                xInd = xIndex,
                yInd = yIndex
            };
        }

        private struct NearestIndexes
        {
            public int xInd;
            public int yInd;
        }

        private HierarchicCluster UniteClusters(HierarchicCluster a, HierarchicCluster b)
        {
            var res = new HierarchicCluster();

            a.Reference = res;
            b.Reference = res;
            res.DocumentLinks = new Collection<DocumentClusterLink> (a.DocumentLinks.Concat(b.DocumentLinks).Select(oldLink => new DocumentClusterLink
                {
                    Document = oldLink.Document,
                    Cluster = res,
                    GradeOfMembership = null
                }).ToList());
            return res;
        }
    }
}
