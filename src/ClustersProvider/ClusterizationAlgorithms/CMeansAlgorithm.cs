﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Data.Models;
using Data.Models.Clusters;
using ClustersProvider.Interfaces;

namespace ClustersProvider.ClusterizationAlgorithms
{
    public class CMeansAlgorithm : MeansAlgorithm
    {
        private static Random rand = new Random();

        private readonly double fuzzyDegree;

        public CMeansAlgorithm(IDocumentCompare comparator, ISeedChoose seeder, int clustersCount, int maxIterations, double fuzzyDegree)
            : base(comparator, seeder, clustersCount, maxIterations)
        {
            if (fuzzyDegree < 1)
                throw new ArgumentOutOfRangeException("fuzzyDegree");
            this.fuzzyDegree = 2 / (fuzzyDegree - 1);

            createdSet = new MeansClusterSet()
            {
                ClusterizationAlgorithmId = ClusterizationAlgorithmType.Cmeans,
                ClustersNumber = clustersCount
            };
        }

        private readonly ClusterSet createdSet;
        public override ClusterSet CreatedSet
        {
            get
            {
                return createdSet;
            }
        }

        public override IEnumerable<Cluster> Clusterize(IEnumerable<Document> documents)
        {
            var docs = documents.OrderBy(doc => doc.Id).ToArray();
            double[][] tempRes = new double[ClusterNumber][];

            for (int i = 0; i < tempRes.Length; ++i)
            {
                tempRes[i] = new double[docs.Length];
                for (int j = 0; j < tempRes[i].Length; ++j)
                    tempRes[i][j] = rand.NextDouble();
            }

            for (int i = 0; i < MaxIterations; ++i)
            {
                var centers = FindCenters(tempRes, docs);
                GetDegrees(tempRes, docs, centers);
            }
            return GetLinks(tempRes, docs);
        }

        private static DocumentTermFactor[][] FindCenters(double[][] grades, Document[] documents)
        {
            var res = new DocumentTermFactor[grades.Length][];
            for (int j = 0; j < grades.Length; ++j)
            {
                double sum = grades[j].Sum();
                res[j] = documents.Zip(grades[j], (doc, docFactor) =>
                    doc.Factors.Select(factor => new DocumentTermFactor() { TermId = factor.TermId, TermFactor = factor.TermFactor * docFactor }))
                    .AsParallel()
                    .SelectMany(x => x)
                    .GroupBy(factor => factor.TermId)
                    .Select(factors => new DocumentTermFactor() { TermId = factors.Key, TermFactor = factors.Sum(factor => factor.TermFactor) / sum })
                    .OrderBy(factor => factor.TermId)
                    .ToArray();
            }
            return res;
        }

        private IEnumerable<Cluster> GetLinks(double[][] tempRes, Document[] docs)
        {
            int mult = tempRes[0].Length;
            var clusters = tempRes.Select(x => new Cluster() { ClusterSet = createdSet }).ToArray();

            for (int i = 0; i < clusters.Length; ++i)
            {
                var temp = new DocumentClusterLink[mult];
                for (int j = 0; j < mult; ++j)
                    temp[j] = new DocumentClusterLink() { Cluster = clusters[i], Document = docs[j], GradeOfMembership = tempRes[i][j] };
                clusters[i].DocumentLinks = temp;
            }
            return clusters;
        }

        private void GetDegrees(double[][] tempRes, Document[] docs, DocumentTermFactor[][] centers)
        {
            for (int i = 0; i < tempRes.Length; ++i)
                for (int j = 0; j < tempRes[i].Length; ++j)
                {
                    var currDist = DocumentComparator.Distance(docs[j].Factors, centers[i]);
                    tempRes[i][j] = centers.Sum(center => Math.Pow(currDist / DocumentComparator.Distance(docs[j].Factors, center), fuzzyDegree));
                }
        }
    }
}
