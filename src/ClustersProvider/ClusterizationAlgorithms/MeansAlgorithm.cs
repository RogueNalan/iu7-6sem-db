﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Models;
using Data.Models.Clusters;
using ClustersProvider.Interfaces;

namespace ClustersProvider.ClusterizationAlgorithms
{
    public abstract class MeansAlgorithm : AbstractClusterizationAlgorithm
    {
        protected ISeedChoose Seeder { get { return seeder; } }
        private readonly ISeedChoose seeder;

        protected int ClusterNumber { get { return clusterNumber; } } 
        private readonly int clusterNumber;

        protected int MaxIterations { get { return maxIterations; } }
        private readonly int maxIterations;

        public MeansAlgorithm(IDocumentCompare comparator, ISeedChoose seeder, int clusterNumber, int maxIterations)
            : base(comparator)
        {
            if (clusterNumber <= 0)
                throw new ArgumentException("Number of clusters cannot be less than one!");

            if (seeder == null)
                throw new ArgumentNullException("Seeder cannot be null!");

            this.seeder = seeder;
            this.clusterNumber = clusterNumber;
            this.maxIterations = maxIterations;
        }
    }
}
