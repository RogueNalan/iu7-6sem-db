﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClustersProvider.Interfaces;
using Data.Models;
using Data.Models.Clusters;
using Data.Storage;

namespace ClustersProvider.ClusterizationAlgorithms
{
    public abstract class AbstractClusterizationAlgorithm
    {
        protected IDocumentCompare DocumentComparator { get { return documentComparator; } }
        private readonly IDocumentCompare documentComparator;

        public AbstractClusterizationAlgorithm (IDocumentCompare comparator)
        {
            if (comparator == null)
                throw new ArgumentNullException("Document comparator cannot be null!");

            documentComparator = comparator;
        }

        public abstract ClusterSet CreatedSet { get; }

        public abstract IEnumerable<Cluster> Clusterize(IEnumerable<Document> documents);
    }
}
