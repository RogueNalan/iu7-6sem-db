﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClustersProvider.Interfaces;
using Data.Models;

namespace ClustersProvider.ClusterizationAlgorithms
{
    public class RandomSeeder : ISeedChoose
    {
        private Random random = new Random();

        public IEnumerable<Document> Seed(IList<Document> documents, int count)
        {
            var res = new Document[count];

            int curr = 0;
            while (curr < count)
            {
                int index = random.Next(documents.Count);
                if (!Array.Exists(res, doc => Object.ReferenceEquals(doc, documents[index])))
                    res[curr++] = documents[index];
            }
            return res;
        }
    }
}
