﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using ClustersProvider.Interfaces;
using Data.Models;
using Data.Models.Clusters;

namespace ClustersProvider.ClusterizationAlgorithms
{
    // EM-алгоритм
    public sealed class KMeansAlgorithm : MeansAlgorithm
    {
        public KMeansAlgorithm(IDocumentCompare comparator, ISeedChoose seeder, int clusterNumber, int maxIterarions)
            : base(comparator, seeder, clusterNumber, maxIterarions)
        {
            createdSet = new MeansClusterSet
            {
                ClusterizationAlgorithmId = ClusterizationAlgorithmType.Kmeans,
                ClustersNumber = clusterNumber
            };
        }

        public override ClusterSet CreatedSet { get { return createdSet; } }
        private readonly MeansClusterSet createdSet;

        public override IEnumerable<Cluster> Clusterize(IEnumerable<Document> documents)
        {
            var docs = documents.ToArray();
            var seeds = Seeder.Seed(docs, ClusterNumber).OrderBy(seed => seed.Id).ToArray();

            List<Document>[] clusterSources = null;
            for (int i = 0; i < MaxIterations; ++i)
            {
                clusterSources = Expectation(docs, seeds);
                var newSeeds = Maximization(clusterSources).OrderBy(doc => doc.Id).ToArray();

                if (!newSeeds.SequenceEqual(seeds))
                    seeds = newSeeds;
                else
                    break;
            }

            var clusters = clusterSources.Select(source => new Cluster()).ToList();
            return SetLinks(clusters, clusterSources);
        }

        private List<Document>[] Expectation(Document[] docs, IList<Document> seeds)
        {
            var res = new List<Document>[seeds.Count];
            for (int i = 0; i < res.Length; ++i)
                res[i] = new List<Document>();

            foreach (var doc in docs)
            {
                int minInd = FindClusterIndex(doc, seeds);
                res[minInd].Add(doc);
            }

            return res;
        }

        private Document[] Maximization(IList<Document>[] clusters)
        {
            var res = new Document[clusters.Length];
            for (int i = 0; i < clusters.Length; ++i)
            {
                Document minDoc = FindCenter(clusters[i]);
                res[i] = minDoc;
            }
            return res;
        }

        private int FindClusterIndex(Document doc, IList<Document> clusterSeeds)
        {
            int minInd = 0;
            double minDist = DocumentComparator.Distance(doc.Factors, clusterSeeds[0].Factors);

            for (int i = 1; i < clusterSeeds.Count; ++i)
            {
                double curr = DocumentComparator.Distance(doc.Factors, clusterSeeds[i].Factors);
                if (curr < minDist)
                {
                    minDist = curr;
                    minInd = i;
                }
            }
            return minInd;
        }

        private Document FindCenter(IList<Document> cluster)
        {
            Document minDoc = cluster[0];
            double minSum = cluster.Sum(doc => DocumentComparator.Distance(minDoc.Factors, doc.Factors));

            foreach (var currDoc in cluster)
            {
                double currSum = cluster.Sum(doc => DocumentComparator.Distance(currDoc.Factors, doc.Factors));
                if (currSum < minSum)
                {
                    minDoc = currDoc;
                    minSum = currSum;
                }
            }
            return minDoc;
        }

        private IEnumerable<Cluster> SetLinks(IList<Cluster> clusters, IList<IEnumerable<Document>> clusterSources)
        {
            for (int i = 0; i < ClusterNumber; ++i)
                clusters[i].DocumentLinks = new Collection<DocumentClusterLink>
                (
                    clusterSources[i].Select(source => new DocumentClusterLink
                    {
                        DocumentId = source.Id,
                        Cluster = clusters[i],
                        GradeOfMembership = null
                    }).ToList()
                );
            return clusters;
        }
    }
}
