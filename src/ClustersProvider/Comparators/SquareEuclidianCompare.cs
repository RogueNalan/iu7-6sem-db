﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClustersProvider.Comparators
{
    public class SquareEuclidianCompare : AbstractSummatorCompare
    {
        protected override double SumFunction(double x, double y)
        {
            double temp = y - x;
            return temp * temp;
        }

        protected override double ResultFunction(double res)
        {
            return res;
        }
    }
}
