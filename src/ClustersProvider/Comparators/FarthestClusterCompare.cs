﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Models.Clusters;
using ClustersProvider.Interfaces;

namespace ClustersProvider.Comparators
{
    public class FarthestClusterCompare : AbstractClusterCompare
    {
        public FarthestClusterCompare(IDocumentCompare comparator)
            : base(comparator)
        {
        }

        public override double Distance(Cluster a, Cluster b)
        {
            return a.DocumentLinks.Max(docA =>
                b.DocumentLinks.Max(docB =>
                    DocumentComparator.Distance(docA.Document.Factors, docB.Document.Factors)));
        }
    }
}
