﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClustersProvider.Interfaces;
using Data.Models.Clusters;

namespace ClustersProvider.Comparators
{
    public abstract class AbstractClusterCompare : IClusterCompare
    {
        protected IDocumentCompare DocumentComparator { get { return documentComparator; } }
        private readonly IDocumentCompare documentComparator;

        public AbstractClusterCompare(IDocumentCompare comparator)
        {
            if (comparator == null)
                throw new ArgumentNullException("DocumentComparator cannot be null!");

            documentComparator = comparator;
        }

        public abstract double Distance(Cluster a, Cluster b);
    }
}
