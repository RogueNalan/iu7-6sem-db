﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClustersProvider.Interfaces;
using Data.Models.Clusters;

namespace ClustersProvider.Comparators
{
    public class NearestClusterCompare : AbstractClusterCompare
    {
        public NearestClusterCompare (IDocumentCompare comparator)
            : base(comparator)
        {
        }

        public override double Distance(Cluster a, Cluster b)
        {
            return a.DocumentLinks.Min(docA =>
                b.DocumentLinks.Min(docB =>
                    DocumentComparator.Distance(docA.Document.Factors, docB.Document.Factors)));
        }
    }
}
