﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClustersProvider.Comparators
{
    public class ManhattanCompare : AbstractSummatorCompare
    {
        protected override double SumFunction(double x, double y)
        {
            return Math.Abs(y - x);
        }

        protected override double ResultFunction(double res)
        {
            return res;
        }
    }
}
