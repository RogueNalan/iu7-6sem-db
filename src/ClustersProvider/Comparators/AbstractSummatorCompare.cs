﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClustersProvider.Interfaces;
using Data.Models;

namespace ClustersProvider.Comparators
{
    public abstract class AbstractSummatorCompare : IDocumentCompare
    {
        public double Distance(IEnumerable<DocumentTermFactor> vecA, IEnumerable<DocumentTermFactor> vecB)
        {
            using (var enumerA = vecA.GetEnumerator())
            using (var enumerB = vecB.GetEnumerator())
            {
                bool aNotEnd = enumerA.MoveNext();
                bool bNotEnd = enumerB.MoveNext();

                double res = 0;

                while (aNotEnd && bNotEnd)
                {
                    if (enumerA.Current.TermId < enumerB.Current.TermId)
                    {
                        res += SumFunction(enumerA.Current.TermFactor);
                        aNotEnd = enumerA.MoveNext();
                    }
                    else if (enumerA.Current.TermId > enumerB.Current.TermId)
                    {
                        res += SumFunction(enumerB.Current.TermFactor);
                        bNotEnd = enumerB.MoveNext();
                    }
                    else
                    {
                        res += SumFunction(enumerA.Current.TermFactor, enumerB.Current.TermFactor);
                        aNotEnd = enumerA.MoveNext();
                        bNotEnd = enumerB.MoveNext();
                    }
                }

                if (aNotEnd)
                    res += SumRest(enumerA);
                if (bNotEnd)
                    res += SumRest(enumerB);

                return ResultFunction(res);
            }
        }

        private double SumRest(IEnumerator<DocumentTermFactor> enumer)
        {
            double res = 0;
            do
            {
                res += SumFunction(enumer.Current.TermFactor);
            }
            while (enumer.MoveNext());
            return res;
        }

        protected abstract double SumFunction(double x, double y = 0);

        protected abstract double ResultFunction(double res);
    }
}