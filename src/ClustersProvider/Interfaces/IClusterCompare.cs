﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Models.Clusters;

namespace ClustersProvider.Interfaces
{
    public interface IClusterCompare
    {
        double Distance(Cluster a, Cluster b);
    }
}
