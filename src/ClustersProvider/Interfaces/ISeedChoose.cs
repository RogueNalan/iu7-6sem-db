﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Models;

namespace ClustersProvider.Interfaces
{
    public interface ISeedChoose
    {
        IEnumerable<Document> Seed(IList<Document> documents, int count);
    }
}
