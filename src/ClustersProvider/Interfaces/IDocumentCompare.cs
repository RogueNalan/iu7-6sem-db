﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Data.Models;

namespace ClustersProvider.Interfaces
{
    public interface IDocumentCompare
    {
        double Distance(IEnumerable<DocumentTermFactor> vecA, IEnumerable<DocumentTermFactor> vecB);
    }
}
