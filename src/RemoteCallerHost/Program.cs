﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using RemoteCaller;

namespace RemoteCallerHost
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string hostIP = @"http://" + ConfigReader.HostIpString;
                Uri termsExtractorAddress = new Uri(hostIP + @":52000/RemoteCaller");

                var host = new ServiceHost(typeof(RemoteCallerService), termsExtractorAddress);

                var binding = new WSHttpBinding();
                binding.Security.Mode = SecurityMode.None;
                host.AddServiceEndpoint(typeof(ITermsExtractor), binding, hostIP + @":52001/TermsExtractor");

                binding = new WSHttpBinding();
                binding.Security.Mode = SecurityMode.None;
                host.AddServiceEndpoint(typeof(IClusterizer), binding, hostIP + @":52002/Clusterizer");

                var beh = new ServiceMetadataBehavior();
                beh.HttpGetEnabled = true;
                host.Description.Behaviors.Add(beh);

                host.Open();
                Console.WriteLine("Service is alive!");
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
