﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace RemoteCallerHost
{
    static class ConfigReader
    {
        public static IPAddress HostIp
        {
            get
            { 
                return hostIp;
            }
        }

        public static string HostIpString
        {
            get
            {
                return hostIp.ToString();
            }
        }

        private static readonly IPAddress hostIp = IPAddress.Parse(ConfigurationManager.AppSettings["HostIp"]);
    }
}
