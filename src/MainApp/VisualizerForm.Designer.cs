﻿namespace MainApp
{
	partial class VisualizerForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if ( disposing && (components != null) )
			{
				components.Dispose();
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.glControl1 = new OpenTK.GLControl();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.нормализоватьToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.методToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.springElectricalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.evenDistributionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.menuStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// glControl1
			// 
			this.glControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.glControl1.BackColor = System.Drawing.Color.Black;
			this.glControl1.Location = new System.Drawing.Point(9, 24);
			this.glControl1.Margin = new System.Windows.Forms.Padding(0);
			this.glControl1.Name = "glControl1";
			this.glControl1.Size = new System.Drawing.Size(512, 512);
			this.glControl1.TabIndex = 29;
			this.glControl1.VSync = true;
			this.glControl1.Load += new System.EventHandler(this.glControl1_Load);
			this.glControl1.Paint += new System.Windows.Forms.PaintEventHandler(this.glControl1_Paint);
			this.glControl1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseClick);
			this.glControl1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseDown);
			this.glControl1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseMove);
			this.glControl1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.glControl1_MouseUp);
			this.glControl1.Resize += new System.EventHandler(this.glControl1_Resize);
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.нормализоватьToolStripMenuItem,
            this.методToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(530, 24);
			this.menuStrip1.TabIndex = 30;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// нормализоватьToolStripMenuItem
			// 
			this.нормализоватьToolStripMenuItem.Name = "нормализоватьToolStripMenuItem";
			this.нормализоватьToolStripMenuItem.Size = new System.Drawing.Size(97, 20);
			this.нормализоватьToolStripMenuItem.Text = "Нормализовать";
			this.нормализоватьToolStripMenuItem.Click += new System.EventHandler(this.btnNormalizeGraph_Click);
			// 
			// методToolStripMenuItem
			// 
			this.методToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.springElectricalToolStripMenuItem,
            this.evenDistributionToolStripMenuItem});
			this.методToolStripMenuItem.Name = "методToolStripMenuItem";
			this.методToolStripMenuItem.Size = new System.Drawing.Size(64, 20);
			this.методToolStripMenuItem.Text = "Метод...";
			// 
			// springElectricalToolStripMenuItem
			// 
			this.springElectricalToolStripMenuItem.CheckOnClick = true;
			this.springElectricalToolStripMenuItem.Name = "springElectricalToolStripMenuItem";
			this.springElectricalToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.springElectricalToolStripMenuItem.Text = "Spring-Electrical";
			this.springElectricalToolStripMenuItem.Click += new System.EventHandler(this.method_Click);
			// 
			// evenDistributionToolStripMenuItem
			// 
			this.evenDistributionToolStripMenuItem.CheckOnClick = true;
			this.evenDistributionToolStripMenuItem.Name = "evenDistributionToolStripMenuItem";
			this.evenDistributionToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
			this.evenDistributionToolStripMenuItem.Text = "Even Distribution";
			this.evenDistributionToolStripMenuItem.Click += new System.EventHandler(this.method_Click);
			// 
			// VisualizerForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(530, 545);
			this.Controls.Add(this.glControl1);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.MinimumSize = new System.Drawing.Size(538, 572);
			this.Name = "VisualizerForm";
			this.Text = "Documents graph";
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private OpenTK.GLControl glControl1;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem нормализоватьToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem методToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem springElectricalToolStripMenuItem;
		private System.Windows.Forms.ToolStripMenuItem evenDistributionToolStripMenuItem;
	}
}