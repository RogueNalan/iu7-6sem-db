﻿using DocumentFilesProcessor;
using MainApp.RemoteServerReferece;
using System;
using System.Windows.Forms;

namespace MainApp
{
    public partial class MainAppForm : Form
    {
        public MainAppForm()
        {
            InitializeComponent();
			InitializeVisualizeTab();
        }

        private void btnSplitFiles_Click(object sender, EventArgs e)
        {
            string src = null;

            var res = folderBrowserDialog.ShowDialog();
            if (res != DialogResult.OK)
                return;

            src = folderBrowserDialog.SelectedPath;

            res = folderBrowserDialog.ShowDialog();
            if (res != DialogResult.OK)
                return;

            try
            {
                new DocumentsDirectoryProcessor(src).ParseDirectoryParallel(folderBrowserDialog.SelectedPath);
                MessageBox.Show("Operation completed succesfully!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + "\n\n" +ex.StackTrace);
            }
        }

        private void btnProcessDocuments_Click(object sender, EventArgs e)
        {
            var res = folderBrowserDialog.ShowDialog();
            if (res != DialogResult.OK)
                return;

            var proc = new DocumentsDirectoryProcessor(folderBrowserDialog.SelectedPath);
            proc.ClearTextsParallel();
            proc.AddToDatabase();

            MessageBox.Show("Operation completed succesfully!");
        }

        private void btnTermsProvider_Click(object sender, EventArgs e)
        {
            try
            {
                var client = new TermsExtractorClient();
                client.ExtractTerms();
                client.Close();

                MessageBox.Show("Operation completed succesfully!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnKTest_Click(object sender, EventArgs e)
        {
            try
            {
                var client = new ClusterizerClient();
                client.KMeans(int.Parse(tbKClusters.Text), int.Parse(tbKIter.Text));
                client.Close();

                MessageBox.Show("Operation completed succesfully!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnHier_Click(object sender, EventArgs e)
        {
            try
            {
                var client = new ClusterizerClient();
                client.Hierarchic(int.Parse(tbHMinClust.Text));
                client.Close();

                MessageBox.Show("Operation completed succesfully!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
	}
}
