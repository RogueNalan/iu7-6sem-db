﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Threading.Tasks;

namespace MainApp
{
	public partial class MainAppForm : Form
	{
		void InitializeVisualizeTab()
		{
			progressBar.Value = 0;
			lblProgressText.Text = "";
		}

		void EnableControls(bool state)
		{
			foreach ( Control c in tabPage_Vis.Controls )
			{
				if ( c is Button )
					c.Enabled = state;
			}
		}
		void ResetProgress()
		{
			lblProgressText.Text = "Подготовка...";
			txtLogger.Text = "";
		}




		private async void btnRefpos_Click(object sender, EventArgs e)
		{
			ResetProgress();
			EnableControls( false );

			using ( var dlp = new DocumentLinkProcessor.DocumentLinkProcessor( this ) )
			{
				await dlp.findRefpos();
			}

			EnableControls( true );
		}

		private async void btnMagic_Click(object sender, EventArgs e)
		{
			ResetProgress();
			EnableControls( false );

			using ( var dlp = new DocumentLinkProcessor.DocumentLinkProcessor( this ) )
			{
				await dlp.doMagic();
			}

			EnableControls( true );
		}

		private async void btnCreateGraph_Click(object sender, EventArgs e)
		{
			ResetProgress();
			EnableControls( false );

			Graphite.Graph g;
			using ( var dlp = new DocumentLinkProcessor.DocumentLinkProcessor( this ) )
			{
				Task<Graphite.Graph> t = dlp.CreateReferenceGraph();
				await t;
				g = t.Result;
			}
			new VisualizerForm( g ).Show();

			EnableControls( true );
		}
	}
}
