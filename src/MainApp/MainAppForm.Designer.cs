﻿namespace MainApp
{
    partial class MainAppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.btnSplitFiles = new System.Windows.Forms.Button();
			this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
			this.btnProcessDocuments = new System.Windows.Forms.Button();
			this.btnTermsProvider = new System.Windows.Forms.Button();
			this.btnKTest = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tbKIter = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tbKClusters = new System.Windows.Forms.TextBox();
			this.btnHier = new System.Windows.Forms.Button();
			this.label3 = new System.Windows.Forms.Label();
			this.tbHMinClust = new System.Windows.Forms.TextBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage_Vis = new System.Windows.Forms.TabPage();
			this.button2 = new System.Windows.Forms.Button();
			this.btnRefpos = new System.Windows.Forms.Button();
			this.lblProgressText = new System.Windows.Forms.Label();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.txtLogger = new System.Windows.Forms.TextBox();
			this.btnMagic = new System.Windows.Forms.Button();
			this.tabPage_Data = new System.Windows.Forms.TabPage();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.toolTip_findRefPos = new System.Windows.Forms.ToolTip(this.components);
			this.toolTip_processReferences = new System.Windows.Forms.ToolTip(this.components);
			this.toolTip_createGraph = new System.Windows.Forms.ToolTip(this.components);
			this.tabControl1.SuspendLayout();
			this.tabPage_Vis.SuspendLayout();
			this.tabPage_Data.SuspendLayout();
			this.SuspendLayout();
			// 
			// btnSplitFiles
			// 
			this.btnSplitFiles.Location = new System.Drawing.Point(6, 6);
			this.btnSplitFiles.Name = "btnSplitFiles";
			this.btnSplitFiles.Size = new System.Drawing.Size(134, 23);
			this.btnSplitFiles.TabIndex = 0;
			this.btnSplitFiles.Text = "Разбить документы";
			this.btnSplitFiles.UseVisualStyleBackColor = true;
			this.btnSplitFiles.Click += new System.EventHandler(this.btnSplitFiles_Click);
			// 
			// folderBrowserDialog
			// 
			this.folderBrowserDialog.RootFolder = System.Environment.SpecialFolder.MyComputer;
			// 
			// btnProcessDocuments
			// 
			this.btnProcessDocuments.Location = new System.Drawing.Point(6, 35);
			this.btnProcessDocuments.Name = "btnProcessDocuments";
			this.btnProcessDocuments.Size = new System.Drawing.Size(134, 23);
			this.btnProcessDocuments.TabIndex = 1;
			this.btnProcessDocuments.Text = "Обработать документы";
			this.btnProcessDocuments.UseVisualStyleBackColor = true;
			this.btnProcessDocuments.Click += new System.EventHandler(this.btnProcessDocuments_Click);
			// 
			// btnTermsProvider
			// 
			this.btnTermsProvider.Location = new System.Drawing.Point(157, 6);
			this.btnTermsProvider.Name = "btnTermsProvider";
			this.btnTermsProvider.Size = new System.Drawing.Size(172, 23);
			this.btnTermsProvider.TabIndex = 2;
			this.btnTermsProvider.Text = "Выделить термины";
			this.btnTermsProvider.UseVisualStyleBackColor = true;
			this.btnTermsProvider.Click += new System.EventHandler(this.btnTermsProvider_Click);
			// 
			// btnKTest
			// 
			this.btnKTest.Location = new System.Drawing.Point(157, 35);
			this.btnKTest.Name = "btnKTest";
			this.btnKTest.Size = new System.Drawing.Size(172, 23);
			this.btnKTest.TabIndex = 3;
			this.btnKTest.Text = "Запустить алгоритм K-средних";
			this.btnKTest.UseVisualStyleBackColor = true;
			this.btnKTest.Click += new System.EventHandler(this.btnKTest_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(341, 40);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(59, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Итераций:";
			// 
			// tbKIter
			// 
			this.tbKIter.Location = new System.Drawing.Point(406, 37);
			this.tbKIter.Name = "tbKIter";
			this.tbKIter.Size = new System.Drawing.Size(40, 20);
			this.tbKIter.TabIndex = 5;
			this.tbKIter.Text = "10";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(452, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(64, 13);
			this.label2.TabIndex = 6;
			this.label2.Text = "Кластеров:";
			// 
			// tbKClusters
			// 
			this.tbKClusters.Location = new System.Drawing.Point(522, 37);
			this.tbKClusters.Name = "tbKClusters";
			this.tbKClusters.Size = new System.Drawing.Size(40, 20);
			this.tbKClusters.TabIndex = 7;
			this.tbKClusters.Text = "15";
			// 
			// btnHier
			// 
			this.btnHier.Location = new System.Drawing.Point(157, 65);
			this.btnHier.Name = "btnHier";
			this.btnHier.Size = new System.Drawing.Size(172, 23);
			this.btnHier.TabIndex = 8;
			this.btnHier.Text = "Запустить алгомер. алг.";
			this.btnHier.UseVisualStyleBackColor = true;
			this.btnHier.Click += new System.EventHandler(this.btnHier_Click);
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(342, 74);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(114, 13);
			this.label3.TabIndex = 9;
			this.label3.Text = "Минимум кластеров:";
			// 
			// tbHMinClust
			// 
			this.tbHMinClust.Location = new System.Drawing.Point(462, 71);
			this.tbHMinClust.Name = "tbHMinClust";
			this.tbHMinClust.Size = new System.Drawing.Size(40, 20);
			this.tbHMinClust.TabIndex = 10;
			this.tbHMinClust.Text = "1";
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage_Vis);
			this.tabControl1.Controls.Add(this.tabPage_Data);
			this.tabControl1.Location = new System.Drawing.Point(1, 1);
			this.tabControl1.Margin = new System.Windows.Forms.Padding(0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.Padding = new System.Drawing.Point(0, 0);
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(575, 476);
			this.tabControl1.TabIndex = 11;
			// 
			// tabPage_Vis
			// 
			this.tabPage_Vis.Controls.Add(this.label5);
			this.tabPage_Vis.Controls.Add(this.label4);
			this.tabPage_Vis.Controls.Add(this.button2);
			this.tabPage_Vis.Controls.Add(this.btnRefpos);
			this.tabPage_Vis.Controls.Add(this.lblProgressText);
			this.tabPage_Vis.Controls.Add(this.progressBar);
			this.tabPage_Vis.Controls.Add(this.txtLogger);
			this.tabPage_Vis.Controls.Add(this.btnMagic);
			this.tabPage_Vis.Location = new System.Drawing.Point(4, 22);
			this.tabPage_Vis.Name = "tabPage_Vis";
			this.tabPage_Vis.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage_Vis.Size = new System.Drawing.Size(567, 450);
			this.tabPage_Vis.TabIndex = 0;
			this.tabPage_Vis.Text = "Vis shit";
			this.tabPage_Vis.UseVisualStyleBackColor = true;
			// 
			// button2
			// 
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
			this.button2.Location = new System.Drawing.Point(418, 6);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(139, 23);
			this.button2.TabIndex = 34;
			this.button2.Text = "Сформировать граф";
			this.toolTip_createGraph.SetToolTip(this.button2, "Создаёт граф на основе выделенных связей между документами");
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.btnCreateGraph_Click);
			// 
			// btnRefpos
			// 
			this.btnRefpos.Location = new System.Drawing.Point(6, 6);
			this.btnRefpos.Name = "btnRefpos";
			this.btnRefpos.Size = new System.Drawing.Size(112, 23);
			this.btnRefpos.TabIndex = 31;
			this.btnRefpos.Text = "Выделить ссылки";
			this.toolTip_findRefPos.SetToolTip(this.btnRefpos, "Выделяет отдельные заголовки из списка литературы для каждого документа");
			this.btnRefpos.UseVisualStyleBackColor = true;
			this.btnRefpos.Click += new System.EventHandler(this.btnRefpos_Click);
			// 
			// lblProgressText
			// 
			this.lblProgressText.AutoSize = true;
			this.lblProgressText.Location = new System.Drawing.Point(15, 40);
			this.lblProgressText.Name = "lblProgressText";
			this.lblProgressText.Size = new System.Drawing.Size(104, 13);
			this.lblProgressText.TabIndex = 30;
			this.lblProgressText.Text = "Обработано доков:";
			this.lblProgressText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// progressBar
			// 
			this.progressBar.Location = new System.Drawing.Point(7, 35);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(550, 23);
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
			this.progressBar.TabIndex = 29;
			this.progressBar.Value = 50;
			// 
			// txtLogger
			// 
			this.txtLogger.Location = new System.Drawing.Point(7, 64);
			this.txtLogger.Multiline = true;
			this.txtLogger.Name = "txtLogger";
			this.txtLogger.ReadOnly = true;
			this.txtLogger.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.txtLogger.Size = new System.Drawing.Size(550, 377);
			this.txtLogger.TabIndex = 27;
			// 
			// btnMagic
			// 
			this.btnMagic.Location = new System.Drawing.Point(208, 6);
			this.btnMagic.Name = "btnMagic";
			this.btnMagic.Size = new System.Drawing.Size(117, 23);
			this.btnMagic.TabIndex = 26;
			this.btnMagic.Text = "Обработать ссылки";
			this.toolTip_processReferences.SetToolTip(this.btnMagic, "Устанавливает связи между документами на основе выделенных заголовков из списка л" +
        "итературы\r\nЕсли в списке присутствует заголовок отсутствующего в базе документа," +
        " создаёт для него фантомный документ");
			this.btnMagic.UseVisualStyleBackColor = true;
			this.btnMagic.Click += new System.EventHandler(this.btnMagic_Click);
			// 
			// tabPage_Data
			// 
			this.tabPage_Data.Controls.Add(this.btnSplitFiles);
			this.tabPage_Data.Controls.Add(this.tbHMinClust);
			this.tabPage_Data.Controls.Add(this.btnProcessDocuments);
			this.tabPage_Data.Controls.Add(this.label3);
			this.tabPage_Data.Controls.Add(this.btnTermsProvider);
			this.tabPage_Data.Controls.Add(this.btnHier);
			this.tabPage_Data.Controls.Add(this.btnKTest);
			this.tabPage_Data.Controls.Add(this.tbKClusters);
			this.tabPage_Data.Controls.Add(this.label1);
			this.tabPage_Data.Controls.Add(this.label2);
			this.tabPage_Data.Controls.Add(this.tbKIter);
			this.tabPage_Data.Location = new System.Drawing.Point(4, 22);
			this.tabPage_Data.Name = "tabPage_Data";
			this.tabPage_Data.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage_Data.Size = new System.Drawing.Size(567, 450);
			this.tabPage_Data.TabIndex = 1;
			this.tabPage_Data.Text = "Data shit";
			this.tabPage_Data.UseVisualStyleBackColor = true;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(151, 11);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(19, 13);
			this.label4.TabIndex = 35;
			this.label4.Text = "-->";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(363, 11);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(19, 13);
			this.label5.TabIndex = 36;
			this.label5.Text = "-->";
			// 
			// MainAppForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(574, 476);
			this.Controls.Add(this.tabControl1);
			this.Name = "MainAppForm";
			this.Text = "TextAnalyzer";
			this.tabControl1.ResumeLayout(false);
			this.tabPage_Vis.ResumeLayout(false);
			this.tabPage_Vis.PerformLayout();
			this.tabPage_Data.ResumeLayout(false);
			this.tabPage_Data.PerformLayout();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnSplitFiles;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.Button btnProcessDocuments;
        private System.Windows.Forms.Button btnTermsProvider;
        private System.Windows.Forms.Button btnKTest;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbKIter;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbKClusters;
        private System.Windows.Forms.Button btnHier;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbHMinClust;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage_Vis;
		private System.Windows.Forms.TextBox txtLogger;
		private System.Windows.Forms.Button btnMagic;
		private System.Windows.Forms.TabPage tabPage_Data;
		private System.Windows.Forms.Label lblProgressText;
		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.Button btnRefpos;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.ToolTip toolTip_createGraph;
		private System.Windows.Forms.ToolTip toolTip_findRefPos;
		private System.Windows.Forms.ToolTip toolTip_processReferences;
	}
}

