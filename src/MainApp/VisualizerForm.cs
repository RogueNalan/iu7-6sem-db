﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Graphite;
using Graphite.DrawingAlgorithms;
using ForceDriven = Graphite.DrawingAlgorithms.ForceDriven;

namespace MainApp
{
	public partial class VisualizerForm : Form
	{
		public VisualizerForm( Graph g )
		{
			InitializeComponent();
			glControl1.MouseWheel += new System.Windows.Forms.MouseEventHandler( glControl1_MouseWheel );
			Graphone.I.g = g;
		}

		AbstractAlgorithm alg;
		private async void btnNormalizeGraph_Click(object sender, EventArgs e)
		{
			if ( alg == null )
			{
				MessageBox.Show( "Арара! выбери алгоритм!" );
				return;
			}

			await Task.Factory.StartNew( () =>
			{
				bool repeat;
				do
				{
					repeat = alg.Apply();
					glControl1.Invalidate();
					//Thread.Sleep( 100 );
				}
				while ( repeat );
			},
				TaskCreationOptions.LongRunning
			);
		}


		/*Graph CreateAgglomerativeGraph()
		{
			var clusters = context.Clusters.Where( c => c is HierarchicCluster );
			var links = context.DocumentClusterLinks.Where( l => clusters.Contains(l.Cluster) );

			Graph g = new Graph();
			Random r = new Random();
			foreach (var l in links)
			{
				Node n = new Node( (float)r.NextDouble()*100, (float)r.NextDouble()*100 );
				n.data = l;
				g.nodes.Add( n );
			}

			return g;
		}*/
#region glControl related	=	=	=	=	=	=	=	=	=	=	=	=
		private void glControl1_Resize(object sender, EventArgs e)
		{
			Graphone.I.Resize();
		}

		private void glControl1_Paint(object sender, PaintEventArgs e)
		{
			Graphone.I.ReDraw();
		}

		private void glControl1_Load(object sender, EventArgs e)
		{
			Graphone.I.Init( glControl1 );
		}

		private bool camMoved = false;
		int lastX, lastY;
		private void glControl1_MouseMove(object sender, MouseEventArgs e)
		{
			if ( e.Button == MouseButtons.Right )
			{
				int dx = e.X - lastX, dy = e.Y - lastY;
				dx = -dx;
				Graphone.I.MoveCam( dx, dy );
				lastX = e.X;
				lastY = e.Y;
				camMoved = true;
			}

			//феерический костыль, чтобы и рисовалось когданадо, и не лагало, и кнопки не невидимились
			glControl1.Invalidate();
		}

		private void glControl1_MouseDown(object sender, MouseEventArgs e)
		{
			if ( e.Button == MouseButtons.Right )
			{
				lastX = e.X;
				lastY = e.Y;
			}
		}

		private void glControl1_MouseUp(object sender, MouseEventArgs e)
		{
			if ( e.Button == MouseButtons.Right )
				camMoved = false;
		}

		private void glControl1_MouseClick(object sender, MouseEventArgs e)
		{
			//а то повадились тут, даже длиннющее зажатие с мышесдвигом всёравно определять как клик
			if ( camMoved )
				return;

			Graphone.I.MouseClick( e.X, e.Y, e.Button );
		}

		private void glControl1_MouseWheel(object sender, MouseEventArgs e)
		{
			//хотя с текущей реализацией, тут даже / ненужно, но пусть будет на всякий случай
			Graphone.I.ZoomCam( e.Delta / 120 ); //почему 120? потому что Control.MouseWheel в мсдн
			glControl1.Invalidate();
		}
#endregion

		private void method_Click(object sender, EventArgs e)
		{
			foreach (ToolStripMenuItem item in методToolStripMenuItem.DropDownItems)
			{
				item.Checked = false;
			}

			((ToolStripMenuItem)sender).Checked = true;
			if ( sender == springElectricalToolStripMenuItem )
				alg = new ForceDriven.SpringElectrical( Graphone.I.g );
			else if ( sender == evenDistributionToolStripMenuItem )
				alg = new ForceDriven.EvenDistribution( Graphone.I.g );

		}
	}
}
