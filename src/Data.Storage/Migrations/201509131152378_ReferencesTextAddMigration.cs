namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReferencesTextAddMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("docs.Documents", "ReferencesText", c => c.Binary());
            DropColumn("docs.Documents", "ReferencesPosition");
        }
        
        public override void Down()
        {
            AddColumn("docs.Documents", "ReferencesPosition", c => c.Int());
            DropColumn("docs.Documents", "ReferencesText");
        }
    }
}
