namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    using Data.Models;
    using Data.Models.Clusters;

    internal sealed class Configuration : DbMigrationsConfiguration<Data.Storage.TextAnalyzerDb>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Data.Storage.TextAnalyzerDb context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            SeedClusterizationAlgorithms(context);
            SeedExtractionAlgorithms(context);
        }

        private void SeedClusterizationAlgorithms(Data.Storage.TextAnalyzerDb context)
        {
            context.ClusterizationAlgorithms.AddOrUpdate
            (
                new ClusterizationAlgorithm { Id = ClusterizationAlgorithmType.Hierarchic, Description = @"Алгомеративная иерархическая кластеризация", IsHierarchic = true, IsFuzzy = false },
                new ClusterizationAlgorithm { Id = ClusterizationAlgorithmType.Kmeans, Description = @"K-средних", IsHierarchic = false, IsFuzzy = false },
                new ClusterizationAlgorithm { Id = ClusterizationAlgorithmType.Cmeans, Description = @"C-средних", IsHierarchic = false, IsFuzzy = true }
            );
        }

        private void SeedExtractionAlgorithms(Data.Storage.TextAnalyzerDb context)
        {
            context.TermExtractionAlgorithms.AddOrUpdate
            (
                new TermExtractionAlgorithm { Id = ExtractionAlgorithmType.SolarixLemmatization, Description = @"Лемматизация библиотекой Solarix" }
            );
        }
    }
}
