namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsPhantomMigration : DbMigration
    {
        public override void Up()
        {
            AddColumn("docs.Documents", "IsPhantom", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("docs.Documents", "IsPhantom");
        }
    }
}
