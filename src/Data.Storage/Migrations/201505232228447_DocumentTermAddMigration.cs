namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DocumentTermAddMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.DocumentClusterLinks", "IX_DocumentClusterLinkFKeys");
            DropPrimaryKey("dbo.DocumentClusterLinks");
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Header = c.String(nullable: false, maxLength: 200),
                        Contents = c.Binary(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Header, unique: true);
            
            CreateTable(
                "dbo.TermFactors",
                c => new
                    {
                        DocumentId = c.Int(nullable: false),
                        TermId = c.Int(nullable: false),
                        TermFactor = c.Single(nullable: false),
                    })
                .PrimaryKey(t => new { t.DocumentId, t.TermId })
                .ForeignKey("dbo.Documents", t => t.DocumentId, cascadeDelete: true)
                .ForeignKey("dbo.Terms", t => t.TermId, cascadeDelete: true)
                .Index(t => t.DocumentId)
                .Index(t => t.TermId);
            
            CreateTable(
                "dbo.Terms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.String(nullable: false, maxLength: 50),
                        DocumentFactor = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Value, unique: true);
            
            AddColumn("dbo.Clusters", "Discriminator", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.DocumentClusterLinks", new[] { "DocumentId", "ClusterId" });
            CreateIndex("dbo.DocumentClusterLinks", "DocumentId");
            CreateIndex("dbo.DocumentClusterLinks", "ClusterId");
            AddForeignKey("dbo.DocumentClusterLinks", "DocumentId", "dbo.Documents", "Id", cascadeDelete: true);
            DropColumn("dbo.DocumentClusterLinks", "Id");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DocumentClusterLinks", "Id", c => c.Int(nullable: false, identity: true));
            DropForeignKey("dbo.TermFactors", "TermId", "dbo.Terms");
            DropForeignKey("dbo.TermFactors", "DocumentId", "dbo.Documents");
            DropForeignKey("dbo.DocumentClusterLinks", "DocumentId", "dbo.Documents");
            DropIndex("dbo.Terms", new[] { "Value" });
            DropIndex("dbo.TermFactors", new[] { "TermId" });
            DropIndex("dbo.TermFactors", new[] { "DocumentId" });
            DropIndex("dbo.Documents", new[] { "Header" });
            DropIndex("dbo.DocumentClusterLinks", new[] { "ClusterId" });
            DropIndex("dbo.DocumentClusterLinks", new[] { "DocumentId" });
            DropPrimaryKey("dbo.DocumentClusterLinks");
            DropColumn("dbo.Clusters", "Discriminator");
            DropTable("dbo.Terms");
            DropTable("dbo.TermFactors");
            DropTable("dbo.Documents");
            AddPrimaryKey("dbo.DocumentClusterLinks", "Id");
            CreateIndex("dbo.DocumentClusterLinks", new[] { "DocumentId", "ClusterId" }, unique: true, name: "IX_DocumentClusterLinkFKeys");
        }
    }
}
