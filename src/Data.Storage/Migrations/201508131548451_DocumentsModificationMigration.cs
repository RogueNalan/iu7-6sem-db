namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DocumentsModificationMigration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "clusterization.DocumentLinks", newName: "DocumentClusterLinks");
            RenameTable(name: "terms.Factors", newName: "DocumentTermFactors");
            MoveTable(name: "clusterization.DocumentClusterLinks", newSchema: "dbo");
            MoveTable(name: "dbo.Documents", newSchema: "docs");
            MoveTable(name: "terms.DocumentTermFactors", newSchema: "dbo");
            CreateTable(
                "docs.DocumentReferences",
                c => new
                    {
                        DocumentId = c.Int(nullable: false),
                        ReferenceDocumentId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.DocumentId, t.ReferenceDocumentId })
                .ForeignKey("docs.Documents", t => t.DocumentId)
                .ForeignKey("docs.Documents", t => t.ReferenceDocumentId)
                .Index(t => t.DocumentId)
                .Index(t => t.ReferenceDocumentId);
            
            AlterColumn("docs.Documents", "Contents", c => c.Binary());
        }
        
        public override void Down()
        {
            DropForeignKey("docs.DocumentReferences", "ReferenceDocumentId", "docs.Documents");
            DropForeignKey("docs.DocumentReferences", "DocumentId", "docs.Documents");
            DropIndex("docs.DocumentReferences", new[] { "ReferenceDocumentId" });
            DropIndex("docs.DocumentReferences", new[] { "DocumentId" });
            AlterColumn("docs.Documents", "Contents", c => c.Binary(nullable: false));
            DropTable("docs.DocumentReferences");
            MoveTable(name: "dbo.DocumentTermFactors", newSchema: "terms");
            MoveTable(name: "docs.Documents", newSchema: "dbo");
            MoveTable(name: "dbo.DocumentClusterLinks", newSchema: "clusterization");
            RenameTable(name: "terms.DocumentTermFactors", newName: "Factors");
            RenameTable(name: "clusterization.DocumentClusterLinks", newName: "DocumentLinks");
        }
    }
}
