namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ClusterizationAlgorithms",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        IsHierarchic = c.Boolean(nullable: false),
                        IsFuzzy = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true);
            
            CreateTable(
                "dbo.Clusters",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClusterizationAlgorithmId = c.Int(nullable: false),
                        ReferenceId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ClusterizationAlgorithms", t => t.ClusterizationAlgorithmId, cascadeDelete: true)
                .ForeignKey("dbo.Clusters", t => t.ReferenceId)
                .Index(t => t.ClusterizationAlgorithmId)
                .Index(t => t.ReferenceId);
            
            CreateTable(
                "dbo.DocumentClusterLinks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocumentId = c.Int(nullable: false),
                        ClusterId = c.Int(nullable: false),
                        GradeOfMembership = c.Single(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clusters", t => t.ClusterId, cascadeDelete: true)
                .Index(t => new { t.DocumentId, t.ClusterId }, unique: true, name: "IX_DocumentClusterLinkFKeys");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DocumentClusterLinks", "ClusterId", "dbo.Clusters");
            DropForeignKey("dbo.Clusters", "ReferenceId", "dbo.Clusters");
            DropForeignKey("dbo.Clusters", "ClusterizationAlgorithmId", "dbo.ClusterizationAlgorithms");
            DropIndex("dbo.DocumentClusterLinks", "IX_DocumentClusterLinkFKeys");
            DropIndex("dbo.Clusters", new[] { "ReferenceId" });
            DropIndex("dbo.Clusters", new[] { "ClusterizationAlgorithmId" });
            DropIndex("dbo.ClusterizationAlgorithms", new[] { "Name" });
            DropTable("dbo.DocumentClusterLinks");
            DropTable("dbo.Clusters");
            DropTable("dbo.ClusterizationAlgorithms");
        }
    }
}
