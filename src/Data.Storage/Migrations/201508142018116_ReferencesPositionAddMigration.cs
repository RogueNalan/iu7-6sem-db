namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReferencesPositionAddMigration : DbMigration
    {
        public override void Up()
        {
            DropIndex("terms.Terms", new[] { "Value" });
            AddColumn("docs.Documents", "ReferencesPosition", c => c.Int());
        }
        
        public override void Down()
        {
            DropColumn("docs.Documents", "ReferencesPosition");
            CreateIndex("terms.Terms", "Value", unique: true);
        }
    }
}
