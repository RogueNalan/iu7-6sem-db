namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SetAddMigration : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ClusterizationAlgorithms", newName: "Algorithms");
            RenameTable(name: "dbo.DocumentClusterLinks", newName: "DocumentLinks");
            RenameTable(name: "dbo.TermFactors", newName: "Factors");
            MoveTable(name: "dbo.Algorithms", newSchema: "clusterization");
            MoveTable(name: "dbo.Clusters", newSchema: "clusterization");
            MoveTable(name: "dbo.DocumentLinks", newSchema: "clusterization");
            MoveTable(name: "dbo.Factors", newSchema: "terms");
            MoveTable(name: "dbo.Terms", newSchema: "terms");
            DropForeignKey("dbo.Clusters", "ClusterizationAlgorithmId", "dbo.ClusterizationAlgorithms");
            DropIndex("clusterization.Algorithms", new[] { "Name" });
            DropIndex("clusterization.Clusters", new[] { "ClusterizationAlgorithmId" });
            CreateTable(
                "clusterization.Sets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ClusterizationAlgorithmId = c.Int(nullable: false),
                        Description = c.String(maxLength: 200),
                        ClustersNumber = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("clusterization.Algorithms", t => t.ClusterizationAlgorithmId, cascadeDelete: true)
                .Index(t => t.ClusterizationAlgorithmId);
            
            CreateTable(
                "terms.Sets",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TermExtractionAlgorithmId = c.Int(nullable: false),
                        Description = c.String(maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("terms.Algorithms", t => t.TermExtractionAlgorithmId, cascadeDelete: true)
                .Index(t => t.TermExtractionAlgorithmId);
            
            CreateTable(
                "terms.Algorithms",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Description, unique: true);
            
            AddColumn("clusterization.Algorithms", "Description", c => c.String(nullable: false, maxLength: 200));
            AddColumn("clusterization.Clusters", "ClusterSetId", c => c.Int(nullable: false));
            AddColumn("clusterization.Clusters", "Description", c => c.String(maxLength: 200));
            AddColumn("terms.Terms", "TermSetId", c => c.Int(nullable: false));
            CreateIndex("clusterization.Algorithms", "Description", unique: true);
            CreateIndex("clusterization.Clusters", "ClusterSetId");
            CreateIndex("terms.Terms", "TermSetId");
            AddForeignKey("clusterization.Clusters", "ClusterSetId", "clusterization.Sets", "Id", cascadeDelete: true);
            AddForeignKey("terms.Terms", "TermSetId", "terms.Sets", "Id", cascadeDelete: true);
            DropColumn("clusterization.Algorithms", "Name");
            DropForeignKey("clusterization.Clusters", "FK_dbo.Clusters_dbo.ClusterizationAlgorithms_ClusterizationAlgorithmId");
            DropColumn("clusterization.Clusters", "ClusterizationAlgorithmId");
        }
        
        public override void Down()
        {
            AddColumn("clusterization.Clusters", "ClusterizationAlgorithmId", c => c.Int(nullable: false));
            AddColumn("clusterization.Algorithms", "Name", c => c.String(nullable: false, maxLength: 200));
            DropForeignKey("terms.Terms", "TermSetId", "terms.Sets");
            DropForeignKey("terms.Sets", "TermExtractionAlgorithmId", "terms.Algorithms");
            DropForeignKey("clusterization.Clusters", "ClusterSetId", "clusterization.Sets");
            DropForeignKey("clusterization.Sets", "ClusterizationAlgorithmId", "clusterization.Algorithms");
            DropIndex("terms.Algorithms", new[] { "Description" });
            DropIndex("terms.Sets", new[] { "TermExtractionAlgorithmId" });
            DropIndex("terms.Terms", new[] { "TermSetId" });
            DropIndex("clusterization.Clusters", new[] { "ClusterSetId" });
            DropIndex("clusterization.Sets", new[] { "ClusterizationAlgorithmId" });
            DropIndex("clusterization.Algorithms", new[] { "Description" });
            DropColumn("terms.Terms", "TermSetId");
            DropColumn("clusterization.Clusters", "Description");
            DropColumn("clusterization.Clusters", "ClusterSetId");
            DropColumn("clusterization.Algorithms", "Description");
            DropTable("terms.Algorithms");
            DropTable("terms.Sets");
            DropTable("clusterization.Sets");
            CreateIndex("clusterization.Clusters", "ClusterizationAlgorithmId");
            CreateIndex("clusterization.Algorithms", "Name", unique: true);
            AddForeignKey("dbo.Clusters", "ClusterizationAlgorithmId", "dbo.ClusterizationAlgorithms", "Id", cascadeDelete: true);
            MoveTable(name: "terms.Terms", newSchema: "dbo");
            MoveTable(name: "terms.Factors", newSchema: "dbo");
            MoveTable(name: "clusterization.DocumentLinks", newSchema: "dbo");
            MoveTable(name: "clusterization.Clusters", newSchema: "dbo");
            MoveTable(name: "clusterization.Algorithms", newSchema: "dbo");
            RenameTable(name: "dbo.Factors", newName: "TermFactors");
            RenameTable(name: "dbo.DocumentLinks", newName: "DocumentClusterLinks");
            RenameTable(name: "dbo.Algorithms", newName: "ClusterizationAlgorithms");
        }
    }
}
