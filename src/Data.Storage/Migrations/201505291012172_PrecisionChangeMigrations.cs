namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrecisionChangeMigrations : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.DocumentClusterLinks", "GradeOfMembership", c => c.Double());
            AlterColumn("dbo.TermFactors", "TermFactor", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TermFactors", "TermFactor", c => c.Single(nullable: false));
            AlterColumn("dbo.DocumentClusterLinks", "GradeOfMembership", c => c.Single());
        }
    }
}
