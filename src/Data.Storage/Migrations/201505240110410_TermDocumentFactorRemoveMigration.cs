namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class TermDocumentFactorRemoveMigration : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Terms", "DocumentFactor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Terms", "DocumentFactor", c => c.Single(nullable: false));
        }
    }
}
