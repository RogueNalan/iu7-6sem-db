namespace Data.Storage.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FieldNamesFixMigration : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "clusterization.Sets", name: "ClusterizationAlgorithmId", newName: "AlgorithmId");
            RenameColumn(table: "terms.Sets", name: "TermExtractionAlgorithmId", newName: "AlgorithmId");
            RenameIndex(table: "clusterization.Sets", name: "IX_ClusterizationAlgorithmId", newName: "IX_AlgorithmId");
            RenameIndex(table: "terms.Sets", name: "IX_TermExtractionAlgorithmId", newName: "IX_AlgorithmId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "terms.Sets", name: "IX_AlgorithmId", newName: "IX_TermExtractionAlgorithmId");
            RenameIndex(table: "clusterization.Sets", name: "IX_AlgorithmId", newName: "IX_ClusterizationAlgorithmId");
            RenameColumn(table: "terms.Sets", name: "AlgorithmId", newName: "TermExtractionAlgorithmId");
            RenameColumn(table: "clusterization.Sets", name: "AlgorithmId", newName: "ClusterizationAlgorithmId");
        }
    }
}
