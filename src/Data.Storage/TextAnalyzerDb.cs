﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;


using Data.Models;
using Data.Models.Clusters;

namespace Data.Storage
{
    public class TextAnalyzerDb : DbContext
    {
        public TextAnalyzerDb()
        {
        }

        public TextAnalyzerDb(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Document>()
                .HasMany(doc => doc.References)
                .WithMany(doc => doc.Citations)
                .Map(map =>
                {
                    map.ToTable("DocumentReferences", "docs");
                    map.MapLeftKey("DocumentId");
                    map.MapRightKey("ReferenceDocumentId");
                });

            modelBuilder.Entity<Document>()
                .HasRequired(d => d.Contents)
                .WithRequiredPrincipal(d => d.Document);
            modelBuilder.Entity<Document>().ToTable("Documents", "docs");
            modelBuilder.Entity<DocContents>().ToTable("Documents", "docs");
        }

        private volatile Type _dependency = typeof(System.Data.Entity.SqlServer.SqlProviderServices); // инициализируем SqlClient

        public DbSet<Document> Documents { get; set; }

        public DbSet<TermExtractionAlgorithm> TermExtractionAlgorithms { get; set; }

        public DbSet<TermSet> TermSets { get; set; }

        public DbSet<Term> Terms { get; set; }

        public DbSet<DocumentTermFactor> TermFactors { get; set; }

        public DbSet<ClusterizationAlgorithm> ClusterizationAlgorithms { get; set; }

        public DbSet<ClusterSet> ClusterSets { get; set; }

        public DbSet<Cluster> Clusters { get; set; }

        public DbSet<DocumentClusterLink> DocumentClusterLinks { get; set; }
    }
}
