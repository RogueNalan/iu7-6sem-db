﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Data.Storage
{
    public static class DbSetExtension
    {
        public static void AddLongRange<T>(this DbSet<T> set, IEnumerable<T> range)
            where T : class
        {
            const int splitLength = 2048;

            var temp = new List<T>(splitLength);

            using (var enumer = range.GetEnumerator())
            {
                while (enumer.MoveNext())
                {
                    temp.Add(enumer.Current);
                    if (temp.Count == splitLength)
                    {
                        set.AddRange(temp);
                        temp.Clear();
                    }
                }
                if (temp.Count != 0)
                    set.AddRange(temp);
            }
        }
    }
}
