﻿using ClustersProvider.ClusterizationAlgorithms;
using ClustersProvider.Comparators;
using Data.Models;
using Data.Models.Clusters;
using Data.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TextAnalyzerWeb.Controllers
{
    public class ClusterSetsController : Controller
    {
        // GET: Clusters
        public ActionResult Index()
        {
            List<ClusterSet> sets;
            using (var ctx = new TextAnalyzerDb())
            {
                sets = ctx.ClusterSets.ToList();
                ClusterizationAlgorithm alg;
                foreach (var set in sets)
                    alg = set.Algorithm;
            }
            return View(sets);
        }

        // GET: Clusters/Details/5z
        public ActionResult Details(int id)
        {
            ICollection<Cluster> clusters;
            using (var ctx = new TextAnalyzerDb())
                clusters = ctx.ClusterSets.FirstOrDefault(x => x.Id == id)?.Clusters;
            return View(clusters);
        }

        public ActionResult ClusterDetails(int id)
        {
            IEnumerable<Document> documents;
            using (var ctx = new TextAnalyzerDb())
                documents = ctx.Clusters.FirstOrDefault(x => x.Id == id)?.DocumentLinks?.Select(x => x.Document).ToList();
            return View(documents);
        }

        // GET: Clusters/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Clusters/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                AbstractClusterizationAlgorithm alg;
                if (collection["type"] == "agglomeratuve")
                {
                    var comp = new SquareEuclidianCompare();
                    alg = new AgglomerativeHierarchicAlgorithm(comp, new NearestClusterCompare(comp), int.Parse(collection["minClusters"]));
                }
                else
                    alg = new KMeansAlgorithm(new SquareEuclidianCompare(), new RandomSeeder(), int.Parse(collection["clusterNumber"]), int.Parse(collection["maxIterations"]));

                using (var ctx = new TextAnalyzerDb())
                {
                    ctx.Clusters.AddRange(alg.Clusterize(ctx.Documents.Where(x => x.Contents != null)));
                    ctx.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Clusters/Delete/5
        public ActionResult Delete(int id)
        {
            ClusterSet set;
            using (var ctx = new TextAnalyzerDb())
                set = ctx.ClusterSets.FirstOrDefault(x => x.Id == id);
            return View(set);
        }

        // POST: Clusters/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                using (var ctx = new TextAnalyzerDb())
                {
                    var set = ctx.ClusterSets.FirstOrDefault(x => x.Id == id);
                    if (set != null)
                    {
                        ctx.ClusterSets.Remove(set);
                        ctx.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
