﻿using Data.Models;
using Data.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using TermsProvider;
using TermsProvider.Solarix;

namespace TextAnalyzerWeb.Controllers
{
    public class TermsController : Controller
    {
        // GET: Terms
        public ActionResult Index()
        {
            List<TermSet> termSets;
            using (var ctx = new TextAnalyzerDb())
            {
                termSets = ctx.TermSets.ToList();
                TermExtractionAlgorithm alg;
                foreach (var set in termSets)
                    alg = set.Algorithm;
            }
            return View(termSets);
        }

        // GET: Terms/Details/5
        public ActionResult Details(int id)
        {
            TermSet set;
            using (var ctx = new TextAnalyzerDb())
            {
                set = ctx.TermSets.FirstOrDefault(x => x.Id == id);
                var alg = set.Algorithm;
                var terms = set.Terms;
            }
            return View(set);
        }

        [HttpGet]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                var parser = new MainTermsProvider(new SolarixLemmatizatorParser(), new FrequencyFactorStrategy());
                Task.Run(() => parser.FillTerms());
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Terms/Delete/5
        public ActionResult Delete(int id)
        {
            TermSet set;
            using (var ctx = new TextAnalyzerDb())
            {
                set = ctx.TermSets.FirstOrDefault(x => x.Id == id);
                var alg = set.Algorithm;
            }
            return View(set);
        }

        // POST: Terms/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                TermSet set;
                using (var ctx = new TextAnalyzerDb())
                {
                    set = ctx.TermSets.FirstOrDefault(x => x.Id == id);
                    if (set != null)
                    {
                        ctx.TermSets.Remove(set);
                        ctx.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpGet]
        public ActionResult DeleteTerm(int id, FormCollection collection)
        {
            {
                try
                {
                    // TODO: Add delete logic here
                    Term term;
                    int setId = 0;
                    using (var ctx = new TextAnalyzerDb())
                    {
                        term = ctx.Terms.FirstOrDefault(x => x.Id == id);
                        if (term != null)
                        {
                            setId = term.Set.Id;
                            ctx.Terms.Remove(term);
                            ctx.SaveChanges();
                        }
                    }
                    return RedirectToAction("Details", new { id = setId });
                }
                catch
                {
                    return View();
                }
            }
        }
    }
}
