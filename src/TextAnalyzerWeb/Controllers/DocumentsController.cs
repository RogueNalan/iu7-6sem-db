﻿using Data.Models;
using Data.Storage;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TextAnalyzerWeb.Controllers
{
    public class DocumentsController : Controller
    {
        // GET: Documents
        [HttpGet]
        public ActionResult Index()
        {
            List<Document> documents;
            using (var ctx = new TextAnalyzerDb())
                documents = ctx.Documents.ToList();
            return View(documents);
        }

        // GET: Documents/Details/5
        public ActionResult Details(int id)
        {
            Document doc;
            using (var ctx = new TextAnalyzerDb())
                doc = ctx.Documents.FirstOrDefault(x => x.Id == id);
            return View(doc);
        }

        // GET: Documents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Documents/Create
        [HttpPost]
        public ActionResult Upload()
        {
            try
            {
                var stream = Request.Files[0].InputStream;
                string header;
                DocContents contents;
                using (var sr = new StreamReader(stream))
                {
                    header = sr.ReadLine();
                    if (header.Length > Document.MaxHeaderLength)
                        header = header.Substring(0, Document.MaxHeaderLength);
                }
                stream.Position = 0;
                using (var br = new BinaryReader(stream))
                    contents = new DocContents() { Contents = br.ReadBytes((int)stream.Length) };
                using (var ctx = new TextAnalyzerDb())
                {
                    ctx.Documents.Add(new Document() { Header = header, Contents = contents });
                    ctx.SaveChanges();
                }
                return RedirectToAction("Index");
            }
            catch
            {
                return RedirectToAction("Index");
            }
        }

        // GET: Documents/Delete/5
        public ActionResult Delete(int id)
        {
            Document doc;
            using (var ctx = new TextAnalyzerDb())
                doc = ctx.Documents.Find(id);
            return View(doc);
        }

        // POST: Documents/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                using (var ctx = new TextAnalyzerDb())
                {
                    var doc = ctx.Documents.FirstOrDefault(x => x.Id == id);
                    if (doc != null)
                    {
                        doc.Contents.Contents = null;
                        ctx.Documents.Remove(doc);
                        ctx.SaveChanges();
                    }
                }
                return RedirectToAction("Index");
            }   
            catch
            {
                return View();
            }
        }
    }
}
