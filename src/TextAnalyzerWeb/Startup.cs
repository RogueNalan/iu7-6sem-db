﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TextAnalyzerWeb.Startup))]
namespace TextAnalyzerWeb
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
